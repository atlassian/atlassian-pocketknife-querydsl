### Data Center Only

This repository is for DC only.

If you are making DC specific changes then they should be made here.  You will need to use your judgement on whether
bug fix and changes should be placed on the cloud version.  The cloud version is hosted internally at [https://stash.atlassian.com/projects/SDCLOUD/repos/atlassian-pocketknife-querydsl](https://stash.atlassian.com/projects/SDCLOUD/repos/atlassian-pocketknife-querydsl/)


# Overview #
This is the querydsl part of the pocketknife suite of libraries, see [https://bitbucket.org/atlassian/atlassian-pocketknife](https://bitbucket.org/atlassian/atlassian-pocketknife) for details of the original project

This component provides utilities and scaffolding for working with [QueryDSL](http://www.querydsl.com/) within the Atlassian ecosystem.

# Contribution #

Tickets are tracked on the [jdog Pocket project](https://jdog.jira-dev.com/projects/POCKET/issues)

All pull requests are expected to have a green branch build in the [pocketknife-querydsl CI build](https://servicedesk-bamboo.internal.atlassian.com/browse/PKQDSL). Automatic releasing via bamboo is not configured at this stage.

When making changes, please remember to also update the changelog.md

# Getting started #
The main components are available in the `com.atlassian.pocketknife.api.querydsl` package.

The main entry point to the library is:

* `com.atlassian.pocketknife.api.querydsl.DatabaseAccessor`

# Maven Integration #
Include the `atlassian-pocketknife-querydsl` dependency e.g.:

    <dependency>
        <groupId>com.atlassian.pocketknife</groupId>
        <artifactId>atlassian-pocketknife-querydsl</artifactId>
        <version>5.1.0</version>
    </dependency>

Note that for Atlassian P2 plugins, it should be included with `compile` scope, to ensure its classes and dependencies are included within your plugin bundle.

# OSGi Integration #
When including `atlassian-pocketknife-querydsl` and dependencies in an OSGi bundle, care should be taken to import the necessary packages, optionally import the product dependent packages and exclude the "never used" packages.

Note that a `*` import ensures that packages such as jsr305 and others have their packages imported (when provided) rather than using the bundled versions.

    <Import-Package>
        <!-- exclude pocketknife bits we don't need -->
        !com.infradna.tool*,
        !net.sf.cglib.proxy,
        !org.jvnet.hudson.annotation_indexer,
        
        <!-- import (alltheotherthings) -->
        *
    </Import-Package>

# Spring Scanner Notes #

When using [spring-scanner](https://bitbucket.org/atlassian/atlassian-spring-scanner) you will need specify that scanning be done on packages from `atlassian-pocketknife-querydsl` as well as from your plugin.

Product appropriate components will be constructed and injected into your plugin's spring context e.g. a `ConnectionProvider`.

The following configuration should be specified:

    <build>
        <plugins>
            <plugin>
                <groupId>com.atlassian.plugin</groupId>
                <artifactId>atlassian-spring-scanner-maven-plugin</artifactId>
                <configuration>
                    <scannedDependencies>
                        <dependency>
                            <groupId>com.atlassian.pocketknife</groupId>
                            <artifactId>atlassian-pocketknife-querydsl</artifactId>
                        </dependency>
                    </scannedDependencies>
                </configuration>
            </plugin>
        </plugins>
    </build>

# Spring Java Config Notes #

When using [Spring Java config](https://developer.atlassian.com/server/framework/atlassian-sdk/spring-java-config/) you will need to import the provided bean configuration, OSGi services, and export some services yourself. See `PocketKnifeQueryDslBeanConfiguration` for more details.

**Note:** Only Spring Java config or Spring Scanner should be used to avoid duplicate beans

# SQL Querying Code Examples #

To get an understanding of QueryDSL syntax in general [look at this QueryDSL documentation ] (http://www.querydsl.com/static/querydsl/latest/reference/html/ch02s03.html) or look at the examples in this repository [QueryDsl Examples](https://bitbucket.org/atlassian/querydsl-examples)

`DatabaseAccessor` is the mechanism to execute SQL statements.  It will obtain connections on your behalf.

    List<Tuple> assortedSmiths = databaseAccessor.run(databaseConnection ->
        databaseConnection
            .select(EMPLOYEE.ID, EMPLOYEE.NAME)
            .from(EMPLOYEE)
            .where(EMPLOYEE.NAME.contains("Smith"))
            .orderBy(EMPLOYEE.NAME.asc()).fetch()
    }

The lifecycle of the connection is managed for you.

Database access in web applications is a precious resource.  In almost all cases you should obtain a connection, read a limited (re : paged) set of results
into memory and then return the connection as fast as possible.  Do not perform long processing of the results while you hold the database
connection open otherwise you will starve the other web request threads of access to the database.  Never obtain 2 or more connections from the
connection pool at the one time otherwise you may cause deadlocks as web request threads compete to get exclusive access to the database connections.

If you need to perform more "database" operations inside a database call then pass the provided `DatabaseConnection` object to your inner methods and re-use it. Doing
this means, that while you are holding the database connection for longer, you wont create deadlock conditions by asking for another connection from te pool while you are actively
holding one.

As a rule try to "materialise" you database reads into memory and releases the connection as soon as possible.  Even if you quickly ask for another connection while processing these
materialised results, you will have increased the "co-operative" load that the connection pool can handle.


Memory is, however, also a precious resource.  Never load large sets of data from the database without considering the "paging" that should be applied to the
results.  How many rows should you read?  How do you get the next set of results?  How many is enough for the user to handle?  You must consider these
when retrieving data in web applications.


# Streaming Database Access

You should only use database streaming when you have a specific use case (say data import or data migration via an upgrade task) typically in a
background (non web request) thread.

The first rule of database streaming club, is don't use database streaming.  The second rule is, don't even think about database streaming.  And once
you have considered both rules closely, then, and only then, can you become a member of the database streaming club.

You can stream the results instead via `com.atlassian.pocketknife.api.querydsl.stream.StreamingQueryFactory`, loading only some rows into memory
at a time (this is JDBC driver specific via fetch size).

    CloseableIterable<Tuple> result = streamingQueryFactory.stream(databaseConnection, () -> databaseConnection.select(EMPLOYEE.ID, EMPLOYEE.FIRST_NAME, EMPLOYEE.LAST_NAME)
                                                   .from(EMPLOYEE)
                                                   .where(EMPLOYEE.LAST_NAME.contains("Smith"))
                                                   .fetch();

A `CloseableIterable` can be iterated on

     for (Tuple tuple : result ) {
        // consume each Tuple
        System.out.println(tuple.get(EMPLOYEE.FIRST_NAME));
     }

Once the stream is consumed, the underlying result set will be closed.  Like any limited resource
operation it is super important that you do this safely, especially if you code throws Exceptions or short cuts out of the result consumption.

Java 7 try () with resources statements make this easy since `CloseableIterable` is `AutoCloseable`.

    try (CloseableIterable<Tuple> result = streamingQueryFactory.stream(databaseConnection, () -> databaseConnection.select(EMPLOYEE.ID, EMPLOYEE.FIRST_NAME, EMPLOYEE.LAST_NAME)
                                                                                   .from(EMPLOYEE)
                                                                                   .where(EMPLOYEE.LAST_NAME.contains("Smith"))
                                                                                   .fetch())
    {
         for (Tuple t : result ) {
            // consume each Tuple
         }
    
    }

A QueryDSL `Tuple` is a raw result object analogous to a JDBC `ResultSet`.  You can map this into specific objects via the `map()` method

    final List<Employee> employees = new ArrayList<>();
    try (CloseableIterable<Tuple> result = createStreamyQuery())
    {
        for (EMPLOYEE e : result.map(tuple ->
            {
                Employee e = new EMPLOYEE(tuple.get(EMPLOYEE.ID));
                e.setFirstname(tuple.get(EMPLOYEE.FIRST_NAME));
                e.setLastname(tuple.get(EMPLOYEE.LAST_NAME));
                return e;
            }
        })
        {
            employees.add(e);
        }
    }

A variation on mapping is when you want only one type of column based on a single QueryDSL table column
    
        CloseableIterator<String> firstNames = result.map(EMPLOYEE.FIRST_NAME);

    
You can obtain just the first row via the `fetchFirst` method.  Since this is a terminal operation, this will close the stream of results
once called.
    
    Option<String> firstName = result.map(EMPLOYEE.FIRST_NAME).fetchFirst();

You can take only a limited set of rows by using the `take` method.  Its comes in two flavours.  The first is a simple number of rows

    CloseableIterator<String> firstNames = result.map(EMPLOYEE.FIRST_NAME).take(50);

The second method is `takeWhile` which has a predicate as an argument and returns rows until the predicate is no longer true


    Predicate<String> containsM = new Predicate<String>()
    {
        @Override
        public boolean apply(final String input)
        {
            return input.contains("M");
        }
    };
    CloseableIterator<String> firstNames = streamyResult.map(EMPLOYEE.FIRST_NAME).takeWhile(containsM);

Both the `take` and `takeWhile` methods terminate the underlying closeable iterable.

There is a `filter` method that will only returns elements that match a predicate/

    CloseableIterator<String> firstNames = streamyResult.map(EMPLOYEE.FIRST_NAME).filter(name -> name.contains("M");

One thing to remember is that limiting your result sets and filtering them is a job BEST done by the SQL database.  The `take` and `filter` methods should only be used
when you are unable to filter and limit in SQL terms.

You can use side effects to process the results via the `foreach` method

        final List<String> firstNames = new ArrayList<>();
        streamyResult.map(EMPLOYEE.FIRST_NAME).foreach(s -> firstNames.add(s));


There are functions to perform a fold on a stream of results

    Integer count = streamyResult.foldLeft(0, (accumulator, tuple) -> return accumulator + 1);

# SQL DML Examples #

SQL inserts, updates and deletes can also be performed by `com.atlassian.pocketknife.api.querydsl.DatabaseConnection`.  You need to give it the entity to act on and
a function that creates and performs the SQL

        Long qId = databaseConnection.insert(QUEUE)
                .set(QUEUE.NAME, queueDef.getName())
                .set(QUEUE.PURPOSE, queueDef.getPurpose())
                .set(QUEUE.CREATED_TIME, System.currentTimeMillis())
                .executeWithKey(QUEUE.ID);


        databaseConnection.delete(MESSAGE).where(MESSAGE.QUEUE_ID.eq(queueId)).execute();


# Entity Code Examples #

The tables in the system are represented by QueryDSL RelationalPath definitions.  This library has added the 'EnhancedRelationalPathBase'
extension class to make declaring these even easier.

    public class MessageTable extends EnhancedRelationalPathBase<MessageTable>
    {
        // Columns
        public final NumberPath<Long> ID = createLongCol("ID").asPrimaryKey().build();
        public final NumberPath<Long> QUEUE_ID = createLongCol("QUEUE_ID").notNull().build();
        public final NumberPath<Long> CREATED_TIME = createLongCol("CREATED_TIME").notNull().build();
        public final NumberPath<Long> EXPIRY_TIME = createLongCol("EXPIRY_TIME").build();
        public final StringPath CONTENT_TYPE = createStringCol("CONTENT_TYPE").notNull().build();
        public final NumberPath<Integer> VERSION = createIntegerCol("VERSION").notNull().build();
        public final NumberPath<Integer> PRIORITY = createIntegerCol("PRIORITY").notNull().build();
        public final StringPath MSG_ID = createStringCol("MSG_ID").notNull().build();
        public final NumberPath<Long> MSG_LENGTH = createLongCol("MSG_LENGTH").notNull().build();
        public final StringPath MSG_DATA = createStringCol("MSG_DATA").build();

        public MessageTable(final String tableName)
        {
            super(MessageTable.class, tableName);
        }
    }

The above defines a primary key and the set of non null columns in the table.

A common pattern is to then have a `Tables` class that declares instances of these RelationPath objects

    /**
     * Our QueryDSL tables
     */
    public class Tables
    {
        private static final String AO = "AO_319474_";  // <-- our AO namespace

        public static final QueueTable QUEUE = new QueueTable(AO + "QUEUE");
        public static final MessageTable MESSAGE = new MessageTable(AO + "MESSAGE");
        public static final MessagePropertyTable MESSAGE_PROPERTY = new MessagePropertyTable(AO + "MESSAGE_PROPERTY");
    }

With static imports you are then able to make your database code more SQL-like

    import static com.code.querydsl.Tables.QUEUE;


    return databaseConnection.select(QUEUE.all()).from(QUEUE)
            .where(QUEUE.NAME.eq(queueName))
            .fetch();



# Decision Notes #

You may be asking why `CloseableIterator` is not a Java 1.8 `Stream`?  This library pre-dates JDK 8 being available and hence
the CloseableIterator was invented.  Its inspired by JDK 1.8 `Stream` no doubt.  However much of the `Predicate` filtering and grouping
that can be done on `Stream`s are in fact better down at the SQL layer rather than in Java.  So these methods have not been added
to CloseableIterator.

The same pre-dated JDK 1.8 rationale applies to the library's use of Guava functions and Fugue Options.  Perhaps in a future
major version we can use Java 8 constructs instead.


# Migrating from PK QDSL 0.x #

The new 2.x versions updates QueryDSL to v4.x and there is a lot of package renaming to be done.

The old `com.mysema` package has been replaced by `com.querydsl`.

The packages of the relational path classes and column classes have moved


    -import com.mysema.query.sql.ColumnMetadata;
    -import com.mysema.query.sql.PrimaryKey;
    -import com.mysema.query.sql.RelationalPathBase;
    -import com.mysema.query.types.Path;
    -import com.mysema.query.types.path.BooleanPath;
    -import com.mysema.query.types.path.DatePath;
    -import com.mysema.query.types.path.DateTimePath;
    -import com.mysema.query.types.path.NumberPath;
    -import com.mysema.query.types.path.StringPath;
    -import com.mysema.query.types.path.TimePath;

    +import com.querydsl.sql.ColumnMetadata;
    +import com.querydsl.sql.PrimaryKey;
    +import com.querydsl.sql.RelationalPathBase;
    +import com.querydsl.core.types.Path;
    +import com.querydsl.core.types.dsl.BooleanPath;
    +import com.querydsl.core.types.dsl.DatePath;
    +import com.querydsl.core.types.dsl.DateTimePath;
    +import com.querydsl.core.types.dsl.NumberPath;
    +import com.querydsl.core.types.dsl.StringPath;
    +import com.querydsl.core.types.dsl.TimePath;

The packages of the general SQL classes have changed as well

    -import com.mysema.query.QueryMetadata;
    -import com.mysema.query.sql.Configuration;
    -import com.mysema.query.sql.SQLListener;
    -import com.mysema.query.sql.SQLSerializer;
    -import com.mysema.query.sql.dml.SQLInsertBatch;
    -import com.mysema.query.sql.dml.SQLMergeBatch;
    -import com.mysema.query.sql.dml.SQLUpdateBatch;
    -import com.mysema.query.types.Expression;
    -import com.mysema.query.types.SubQueryExpression;

    +import com.querydsl.core.QueryMetadata;
    +import com.querydsl.core.types.Expression;
    +import com.querydsl.core.types.SubQueryExpression;
    +import com.querydsl.sql.Configuration;
    +import com.querydsl.sql.SQLListener;
    +import com.querydsl.sql.SQLSerializer;
    +import com.querydsl.sql.dml.SQLInsertBatch;
    +import com.querydsl.sql.dml.SQLMergeBatch;
    +import com.querydsl.sql.dml.SQLUpdateBatch;

For PK QDSL itself the old streaming code has been removed and replaced by the more simple and safe database accessor code

    -com.atlassian.pocketknife.api.querydsl.QueryFactory
    -com.atlassian.pocketknife.api.querydsl.StreamyResult'

    +com.atlassian.pocketknife.api.querydsl.DatabaseAccessor
    +com.atlassian.pocketknife.api.querydsl.DatabaseConnection

You now use the `DatabaseConnection` interface to obtain QueryDSL queries and then let `DatabaseAccessor` run the queries in a connection pool friendly manner.

If you truly need to access 'database streams' you still can via the `com.atlassian.pocketknife.api.querydsl.stream.StreamingQueryFactory` interface.

Read the sections above on this and remember "The first rule of database streaming club, is don't use database streaming.  The second rule is, don't even think about database streaming.
And once you have considered both rules closely, then, and only then, can you become a member of the database streaming club."

Because `DatabaseAccessor` handles connections for you better, the strong need for `ClosePromise` is reduced and it has been moved into the new 'stream' package `com.atlassian.pocketknife.api.querydsl.stream.ClosePromise`

The `com.atlassian.pocketknife.api.querydsl.TransactionalExecutor` has been removed.  Use `DatabaseAccessor.runInTransaction()` instead.

# Internal Developer Testing Notes #

Many of the unit tests are running a real HSQL database behind the scenes.  The setup code for this is contained
within `com.atlassian.pocketknife.internal.querydsl.mocks.TestingContext`which will wire together a representative
set of PKQDSL components.
 
You should also observe the code running in a real product environment.  The `test-plugin` can be used to run
up PKQDSL in RefApp and JIRA.

    atlas-debug --product jira

The url to hit is `http://localhost:2990/jira/rest/pkqdsl/1.0/test`

`com.atlassian.pocketknife.internal.querydsl.testplugin.rest.TestResource` has code in it that drives PKQDSL for real.
