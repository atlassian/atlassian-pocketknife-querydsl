package com.atlassian.pocketknife.test.util.querydsl;

import io.atlassian.fugue.Option;
import com.atlassian.sal.api.rdbms.ConnectionCallback;
import com.atlassian.sal.api.rdbms.TransactionalExecutor;
import com.atlassian.sal.api.rdbms.TransactionalExecutorFactory;
import com.atlassian.sal.spi.HostConnectionAccessor;
import com.google.common.base.Preconditions;

import javax.annotation.Nonnull;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * This ends up call a copy of the SAL implementation to ensure we run in tests as close as possible
 * to how we run in production
 */
public class StandaloneTransactionalExecutorFactory implements TransactionalExecutorFactory {

    private final Option<String> schemaName;
    private Option<Connection> connection;

    public StandaloneTransactionalExecutorFactory(Option<String> schemaName) {
        this.schemaName = schemaName;
    }

    public void setConnection(Option<Connection> connection) {
        this.connection = connection;
    }

    @Override
    public TransactionalExecutor createExecutor() {
        return executor(false, true);
    }

    @Override
    public TransactionalExecutor createExecutor(final boolean readOnly, final boolean newTransaction) {
        return executor(readOnly, newTransaction);
    }

    private TransactionalExecutor executor(final boolean readOnly, final boolean newTransaction) {
        return new StandaloneTransactionalExecutor(new StandaloneHostConnectionAccessor(), readOnly, newTransaction);
    }

    class StandaloneHostConnectionAccessor implements HostConnectionAccessor {
        @Override
        public <A> A execute(boolean readOnly, boolean newTransaction, @Nonnull ConnectionCallback<A> callback) {
            Preconditions.checkState(connection.isDefined());
            Connection connection = StandaloneTransactionalExecutorFactory.this.connection.get();
            // sal wants auto commit off
            try {
                connection.setAutoCommit(false);
            } catch (SQLException e) {
                throw new RuntimeException("How come we cant set auto commit in this test world? SAL demands it!", e);
            }
            return callback.execute(connection);
        }

        @Nonnull
        @Override
        public Option<String> getSchemaName() {
            return schemaName;
        }
    }

}
