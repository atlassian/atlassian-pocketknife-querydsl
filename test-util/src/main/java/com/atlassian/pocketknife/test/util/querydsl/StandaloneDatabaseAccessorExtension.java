package com.atlassian.pocketknife.test.util.querydsl;

import java.sql.DriverManager;
import javax.annotation.Nonnull;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

/**
 * Instantiate this class for use in standalone tests.
 * <p>
 * User specifies the database details via constructor parameters or system properties. If not specified, an in-memory
 * H2 database will be used.
 * <p>
 * {@link DriverManager} provides the JDBC connections - it is up to the user to ensure that the correct JDBC
 * driver is present in the class loader.
 * <p>
 * User is responsible for creating and deleting test schema and data - whatever is currently present in the database
 * will persist.
 */
public class StandaloneDatabaseAccessorExtension extends StandaloneDatabaseAccessorBase implements BeforeEachCallback, AfterEachCallback {

    /**
     * Constructs an accessor which uses connection information from system properties if present, otherwise in memory
     * H2.
     * <p>
     * Tests the connection once, throwing a runtime exception if it cannot be made.
     */
    public StandaloneDatabaseAccessorExtension() {
        super();
    }

    /**
     * Constructs an accessor with user supplied connection info.
     * <p>
     * Tests the connection once, throwing a runtime exception if it cannot be made.
     *
     * @param url      must be resolvable by {@link DriverManager#getConnection(String, String, String)}
     * @param schema   may be empty
     * @param user     may be empty
     * @param password may be empty
     */
    public StandaloneDatabaseAccessorExtension(@Nonnull final String url, @Nonnull final String schema, @Nonnull final String user, @Nonnull final String password) {
        super(url, schema, user, password);
    }

    @Override
    public void beforeEach(ExtensionContext extensionContext) {
        // Nothing at the moment
    }

    @Override
    public void afterEach(ExtensionContext extensionContext) {
        // Nothing at the moment
    }
}
