package com.atlassian.pocketknife.test.util.querydsl;

import com.atlassian.activeobjects.ao.AtlassianTablePrefix;
import com.atlassian.activeobjects.ao.PrefixedSchemaConfiguration;
import com.atlassian.activeobjects.internal.Prefix;
import com.atlassian.activeobjects.internal.SimplePrefix;
import com.atlassian.pocketknife.api.querydsl.schema.DialectProvider.SupportedDatabase;
import com.atlassian.pocketknife.internal.querydsl.cache.PKQCacheClearerImpl;
import com.atlassian.pocketknife.internal.querydsl.configuration.ConfigurationEnrichmentImpl;
import com.atlassian.pocketknife.internal.querydsl.dialect.DefaultDialectConfiguration;
import com.atlassian.pocketknife.internal.querydsl.schema.DefaultSchemaProvider;
import com.atlassian.pocketknife.internal.querydsl.schema.JdbcTableInspector;
import com.atlassian.pocketknife.internal.querydsl.schema.ProductSchemaProvider;
import com.google.common.collect.ImmutableMap;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.annotation.Nonnull;
import net.java.ao.DatabaseProvider;
import net.java.ao.DisposableDataSource;
import net.java.ao.EntityManager;
import net.java.ao.EntityManagerConfiguration;
import net.java.ao.RawEntity;
import net.java.ao.SchemaConfiguration;
import net.java.ao.atlassian.AtlassianFieldNameConverter;
import net.java.ao.atlassian.AtlassianIndexNameConverter;
import net.java.ao.atlassian.AtlassianSequenceNameConverter;
import net.java.ao.atlassian.AtlassianTableNameConverter;
import net.java.ao.atlassian.AtlassianTriggerNameConverter;
import net.java.ao.atlassian.AtlassianUniqueNameConverter;
import net.java.ao.builder.SimpleNameConverters;
import net.java.ao.db.H2DatabaseProvider;
import net.java.ao.db.MySQLDatabaseProvider;
import net.java.ao.db.OracleDatabaseProvider;
import net.java.ao.db.PostgreSQLDatabaseProvider;
import net.java.ao.db.SQLServerDatabaseProvider;
import net.java.ao.schema.NameConverters;
import net.java.ao.schema.TableNameConverter;
import net.java.ao.schema.info.CachingEntityInfoResolverFactory;
import net.java.ao.schema.info.EntityInfoResolverFactory;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Strings.isNullOrEmpty;
import static com.google.common.base.Strings.nullToEmpty;
import static io.atlassian.fugue.Option.option;
import static java.lang.String.format;
import static java.util.Objects.requireNonNull;

/**
 * Extension of {@link StandaloneAoDatabaseAccessorBase} which allows creation of many ActiveObjects {@link RawEntity}.
 * <p>
 * ActiveObjects entities will be created on first invocation. As per parent, the user is responsible for clearing any
 * database objects or data on subsequent invocations.
 */
@SuppressWarnings({"unchecked", "SqlNoDataSourceInspection"})
class StandaloneAoDatabaseAccessorBase {
    private static final Map<SupportedDatabase, Class<? extends DatabaseProvider>> dialects = ImmutableMap.of(
            SupportedDatabase.H2, H2DatabaseProvider.class,
            SupportedDatabase.SQLSERVER, SQLServerDatabaseProvider.class,
            SupportedDatabase.MYSQL, MySQLDatabaseProvider.class,
            SupportedDatabase.ORACLE, OracleDatabaseProvider.class,
            SupportedDatabase.POSTGRESSQL, PostgreSQLDatabaseProvider.class
    );
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(StandaloneAoDatabaseAccessorBase.class);
    private final StandaloneDatabaseAccessorBase accessor;
    private final String prefix;
    private final Class<? extends RawEntity<?>>[] entities;

    private List<String> tableNames;

    /**
     * {@inheritDoc}
     *
     * @param entities ActiveObjects entities to create if not present
     */
    StandaloneAoDatabaseAccessorBase(@Nonnull final StandaloneDatabaseAccessorBase accessor,
                                     @Nonnull final String prefix,
                                     final Class<? extends RawEntity<?>>... entities) {
        this.accessor = accessor;
        this.prefix = requireNonNull(prefix);
        this.entities = entities;
        this.tableNames = init(entities);
    }

    /**
     * Create the AO <code>entities</code> provided, after mapping all the necessary plumbing.
     */
    private List<String> init(final Class<? extends RawEntity<?>>... entities) {

        // retrieve the AO database provider for the flavour

        final SupportedDatabase supportedDatabase = accessor.doWithConnection(connection -> {
            StandaloneTransactionalExecutorFactory executorFactory = new StandaloneTransactionalExecutorFactory(option(accessor.getSchema()));
            ProductSchemaProvider productSchemaProvider = new ProductSchemaProvider(executorFactory);
            JdbcTableInspector tableInspector = new JdbcTableInspector();
            DefaultSchemaProvider schemaProvider = new DefaultSchemaProvider(productSchemaProvider, tableInspector, new PKQCacheClearerImpl(null));
            DefaultDialectConfiguration configuration = new DefaultDialectConfiguration(schemaProvider, new ConfigurationEnrichmentImpl());
            return configuration
                    .getDialectConfig(connection)
                    .getDatabaseInfo()
                    .getSupportedDatabase();
        });
        if (!dialects.containsKey(supportedDatabase)) {
            throw new RuntimeException(format("Unsupported dialect '%s'", supportedDatabase));
        }

        // create the provider via reflection... I'm sure there's a better way... but... java...
        DatabaseProvider databaseProvider;
        try {
            try {
                // see if there is a constructor that takes a DS and schema name
                databaseProvider = dialects.get(supportedDatabase)
                        .getConstructor(DisposableDataSource.class, String.class)
                        .newInstance(new MinimalDisposableDataSource(accessor), accessor.getSchema());
            } catch (NoSuchMethodException e) {
                // ok...just use a constructor with a DS
                databaseProvider = dialects.get(supportedDatabase)
                        .getConstructor(DisposableDataSource.class)
                        .newInstance(new MinimalDisposableDataSource(accessor));
            }
        } catch (Exception e) {
            throw new RuntimeException("unable to instantiate database provider", e);
        }

        // create the AO entity manager just for entity creation
        final EntityManager entityManager = new EntityManager(databaseProvider, new AtlassianEntityManagerConfiguration(prefix));

        // create or update AO entities - it will not delete anything that's already there
        try {
            entityManager.migrate(entities);
        } catch (SQLException e) {
            throw new RuntimeException("unable to migrate AO entities", e);
        }

        TableNameConverter tableNameConverter = entityManager.getTableNameConverter();
        return Arrays.stream(entities).map(tableNameConverter::getName)
                .collect(Collectors.toList());
    }

    void before() {
        cleanSchema();
        this.tableNames = init(entities);
    }

    void after() {
        cleanSchema();
    }

    private void cleanSchema() {
        tableNames.stream().filter(name -> !isNullOrEmpty(nullToEmpty(name).trim())).forEach(this::dropTable);
    }

    /**
     * Drop a table, use in case you don't like to use StandaloneAoDatabaseAccessor as a @Rule
     *
     * @param tableName table name to drop.
     */
    void dropTable(final String tableName) {
        accessor.doWithConnectionVoid(connection -> {
            // try and drop the normally named table
            try {
                logger.debug("drop table " + tableName);
                connection.prepareStatement("drop table " + tableName).execute();
            } catch (SQLException e) {
                // squish as table probably doesn't actually exist
            }

            // try and drop the stupid AO uppercase table
            try {
                logger.debug("drop table \"" + tableName + "\"");
                connection.prepareStatement("drop table \"" + tableName + "\"").execute();
            } catch (SQLException e) {
                // squish as table probably doesn't actually exist
            }

            // also get rid of any oracle sequences
            try {
                logger.debug("drop sequence \"" + tableName + "_ID_SEQ\"");
                connection.prepareStatement("drop sequence \"" + tableName + "_ID_SEQ\"").execute();
            } catch (SQLException e) {
                // squish as probably not oracle
            }
        });
    }

    /**
     * Implementation that closely matches the ao-plugin configuration.
     */
    private static class AtlassianEntityManagerConfiguration implements EntityManagerConfiguration {
        private final Prefix prefix;

        AtlassianEntityManagerConfiguration(@Nonnull final String prefix) {
            this.prefix = new SimplePrefix(requireNonNull(prefix));
        }

        @Override
        public boolean useWeakCache() {
            return false;
        }

        @Override
        public NameConverters getNameConverters() {
            return new SimpleNameConverters(
                    new AtlassianTableNameConverter(new AtlassianTablePrefix(prefix)),
                    new AtlassianFieldNameConverter(),
                    new AtlassianSequenceNameConverter(),
                    new AtlassianTriggerNameConverter(),
                    new AtlassianIndexNameConverter(),
                    new AtlassianUniqueNameConverter()
            );
        }

        @Override
        public SchemaConfiguration getSchemaConfiguration() {
            return new PrefixedSchemaConfiguration(prefix);
        }

        @Override
        public EntityInfoResolverFactory getEntityInfoResolverFactory() {
            return new CachingEntityInfoResolverFactory();
        }
    }

    /**
     * AO compatible data source that provides just a connection and throws an exception on every other operation.
     */
    private static class MinimalDisposableDataSource implements DisposableDataSource {
        final StandaloneDatabaseAccessorBase accessor;

        MinimalDisposableDataSource(final StandaloneDatabaseAccessorBase accessor) {
            this.accessor = accessor;
        }

        @Override
        public Connection getConnection() {
            return accessor.connection();
        }

        @Override
        public Connection getConnection(String username, String password) {
            throw new UnsupportedOperationException();
        }

        @Override
        public PrintWriter getLogWriter() {
            throw new UnsupportedOperationException();
        }

        @Override
        public void setLogWriter(PrintWriter out) {
            throw new UnsupportedOperationException();
        }

        @Override
        public int getLoginTimeout() {
            throw new UnsupportedOperationException();
        }

        @Override
        public void setLoginTimeout(int seconds) {
            throw new UnsupportedOperationException();
        }

        @Override
        public Logger getParentLogger() {
            throw new UnsupportedOperationException();
        }

        @Override
        public void dispose() {
            throw new UnsupportedOperationException();
        }

        @Override
        public <T> T unwrap(Class<T> iface) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean isWrapperFor(Class<?> iface) {
            throw new UnsupportedOperationException();
        }
    }
}
