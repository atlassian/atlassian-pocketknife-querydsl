package com.atlassian.pocketknife.test.util.querydsl;

import com.atlassian.pocketknife.api.querydsl.DatabaseAccessor;
import com.atlassian.pocketknife.api.querydsl.DatabaseConnection;
import com.atlassian.pocketknife.api.querydsl.DatabaseConnectionConverter;
import com.atlassian.pocketknife.api.querydsl.EitherAwareDatabaseAccessor;
import com.atlassian.pocketknife.api.querydsl.OptionalAwareDatabaseAccessor;
import com.atlassian.pocketknife.api.querydsl.configuration.ConfigurationEnrichment;
import com.atlassian.pocketknife.api.querydsl.schema.DialectProvider;
import com.atlassian.pocketknife.api.querydsl.schema.SchemaStateProvider;
import com.atlassian.pocketknife.api.querydsl.stream.StreamingQueryFactory;
import com.atlassian.pocketknife.api.querydsl.util.OnRollback;
import com.atlassian.pocketknife.internal.querydsl.DatabaseAccessorImpl;
import com.atlassian.pocketknife.internal.querydsl.DatabaseConnectionConverterImpl;
import com.atlassian.pocketknife.internal.querydsl.EitherAwareDatabaseAccessorImpl;
import com.atlassian.pocketknife.internal.querydsl.OptionalAwareDatabaseAccessorImpl;
import com.atlassian.pocketknife.internal.querydsl.cache.PKQCacheClearerImpl;
import com.atlassian.pocketknife.internal.querydsl.configuration.ConfigurationEnrichmentImpl;
import com.atlassian.pocketknife.internal.querydsl.dialect.DefaultDialectConfiguration;
import com.atlassian.pocketknife.internal.querydsl.schema.DatabaseSchemaCreation;
import com.atlassian.pocketknife.internal.querydsl.schema.DefaultSchemaProvider;
import com.atlassian.pocketknife.internal.querydsl.schema.JdbcTableInspector;
import com.atlassian.pocketknife.internal.querydsl.schema.ProductSchemaProvider;
import com.atlassian.pocketknife.internal.querydsl.schema.SchemaProvider;
import com.atlassian.pocketknife.internal.querydsl.schema.SchemaStateProviderImpl;
import com.atlassian.pocketknife.internal.querydsl.stream.StreamingQueryFactoryImpl;
import io.atlassian.fugue.Either;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import javax.annotation.Nonnull;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vibur.dbcp.ViburDBCPDataSource;
import org.vibur.dbcp.ViburDBCPException;

import static io.atlassian.fugue.Option.none;
import static io.atlassian.fugue.Option.option;
import static io.atlassian.fugue.Option.some;
import static java.lang.String.format;
import static java.util.Objects.requireNonNull;

/**
 * Instantiate this class for use in standalone tests.
 * <p>
 * User specifies the database details via constructor parameters or system properties. If not specified, an in-memory
 * H2 database will be used.
 * <p>
 * {@link DriverManager} provides the JDBC connections - it is up to the user to ensure that the correct JDBC
 * driver is present in the class loader.
 * <p>
 * User is responsible for creating and deleting test schema and data - whatever is currently present in the database
 * will persist.
 */
public class StandaloneDatabaseAccessorBase implements DatabaseAccessor, OptionalAwareDatabaseAccessor, EitherAwareDatabaseAccessor {

    public static final String SYS_PROP_URL = StandaloneDatabaseAccessorBase.class.getName() + ".url";
    public static final String SYS_PROP_SCHEMA = StandaloneDatabaseAccessorBase.class.getName() + ".schema";
    public static final String SYS_PROP_USER = StandaloneDatabaseAccessorBase.class.getName() + ".user";
    public static final String SYS_PROP_PASSWORD = StandaloneDatabaseAccessorBase.class.getName() + ".password";
    public static final String DEFAULT_URL = "jdbc:h2:mem:database-accessor-test";
    public static final String DEFAULT_SCHEMA = "PUBLIC";
    public static final String DEFAULT_USER = "sa";
    public static final String DEFAULT_PASSWORD = "";

    private static final Logger log = LoggerFactory.getLogger(StandaloneDatabaseAccessorBase.class);

    private final String url;
    private final String schema;
    private final String user;
    private final String password;

    private final DataSource dataSource;
    private final StandaloneApiRuntime apiRuntime;
    private final DatabaseAccessor delegateDatabaseAccessor;
    private final OptionalAwareDatabaseAccessor optionalAwareDatabaseAccessor;
    private final EitherAwareDatabaseAccessor eitherAwareDatabaseAccessor;
    private final StandaloneTransactionalExecutorFactory executorFactory;

    /**
     * Constructs an accessor which uses connection information from system properties if present, otherwise in memory
     * H2.
     * <p>
     * Tests the connection once, throwing a runtime exception if it cannot be made.
     */
    public StandaloneDatabaseAccessorBase() {
        this(
                System.getProperty(SYS_PROP_URL, DEFAULT_URL),
                System.getProperty(SYS_PROP_SCHEMA, DEFAULT_SCHEMA),
                System.getProperty(SYS_PROP_USER, DEFAULT_USER),
                System.getProperty(SYS_PROP_PASSWORD, DEFAULT_PASSWORD)
        );
    }

    /**
     * Constructs an accessor with user supplied connection info.
     * <p>
     * Tests the connection once, throwing a runtime exception if it cannot be made.
     *
     * @param url      must be resolvable by {@link DriverManager#getConnection(String, String, String)}
     * @param schema   may be empty
     * @param user     may be empty
     * @param password may be empty
     */
    public StandaloneDatabaseAccessorBase(@Nonnull final String url, @Nonnull final String schema, @Nonnull final String user, @Nonnull final String password) {
        this.url = requireNonNull(url);
        this.schema = requireNonNull(schema);
        this.user = requireNonNull(user);
        this.password = requireNonNull(password);

        log.info("constructed with url:'{}' schema:'{}' user='{}' password='{}'", url, schema, user, password);

        this.dataSource = dataSource();

        // build the standalone api runtime
        executorFactory = new StandaloneTransactionalExecutorFactory(option(schema));
        SchemaProvider schemaProvider = new DefaultSchemaProvider(new ProductSchemaProvider(executorFactory), new JdbcTableInspector(), new PKQCacheClearerImpl(null));
        ConfigurationEnrichment configurationEnrichment = new ConfigurationEnrichmentImpl();
        DialectProvider dialectProvider = new DefaultDialectConfiguration(schemaProvider, configurationEnrichment);
        DatabaseConnectionConverter connectionConverter = new DatabaseConnectionConverterImpl(dialectProvider);
        StreamingQueryFactory streamingQueryFactory = new StreamingQueryFactoryImpl();
        JdbcTableInspector tableInspector = new JdbcTableInspector();
        ProductSchemaProvider productSchemaProvider = new ProductSchemaProvider(executorFactory);
        SchemaStateProvider schemaStateProvider = new SchemaStateProviderImpl(productSchemaProvider, tableInspector);
        DatabaseSchemaCreation databaseSchemaCreation = () -> {
        };

        // we use a delegate so that we run as much of the same code that will run in production
        delegateDatabaseAccessor = new DatabaseAccessorImpl(connectionConverter, executorFactory, databaseSchemaCreation);
        optionalAwareDatabaseAccessor = new OptionalAwareDatabaseAccessorImpl(this);
        eitherAwareDatabaseAccessor = new EitherAwareDatabaseAccessorImpl(this);

        // tests can now get the "wired" runtime and make API calls as they expect
        // in production
        apiRuntime = new StandaloneApiRuntime() {
            @Override
            public DialectProvider getDialectProvider() {
                return dialectProvider;
            }

            @Override
            public DatabaseConnectionConverter getDatabaseConnectionConverter() {
                return connectionConverter;
            }

            @Override
            public StreamingQueryFactory getStreamingQueryFactory() {
                return streamingQueryFactory;
            }

            @Override
            public SchemaStateProvider getSchemaStateProvider() {
                return schemaStateProvider;
            }

            @Override
            public ConfigurationEnrichment getConfigurationEnrichment() {
                return configurationEnrichment;
            }

            @Override
            public DatabaseAccessor getDatabaseAccessor() {
                return StandaloneDatabaseAccessorBase.this;
            }

            @Override
            public OptionalAwareDatabaseAccessor getOptionalAwareDatabaseAccessor() {
                return StandaloneDatabaseAccessorBase.this;
            }

            @Override
            public EitherAwareDatabaseAccessor getEitherAwareDatabaseAccessor() {
                return StandaloneDatabaseAccessorBase.this;
            }
        };
    }

    private DataSource dataSource() {
        try {
            ViburDBCPDataSource viburDBCPDataSource = new ViburDBCPDataSource();
            viburDBCPDataSource.setJdbcUrl(url);
            viburDBCPDataSource.setUsername(user);
            viburDBCPDataSource.setPassword(password);
            viburDBCPDataSource.setPoolInitialSize(1);
            viburDBCPDataSource.start();
            return viburDBCPDataSource;
        } catch (ViburDBCPException e) {
            throw new RuntimeException(
                    format("unable to connect to database url:'%s' user='%s' password='%s'", url, user, password),
                    e
            );
        }
    }

    /**
     * @return the API runtime that has been put in place to provide this StandaloneDatabaseAccessor and the general
     * testing environment.  This of this as a analogue to the Spring runtime you get in production.
     */
    public StandaloneApiRuntime getApiRuntime() {
        return apiRuntime;
    }

    @Override
    public <T> T runInNewTransaction(Function<DatabaseConnection, T> callback, OnRollback onRollback) {
        return doWithConnection(connection -> delegateCall(connection, callback, true, onRollback));
    }

    @Override
    public <T> T runInTransaction(Function<DatabaseConnection, T> callback, OnRollback onRollback) {
        return doWithConnection(connection -> delegateCall(connection, callback, false, onRollback));
    }

    @Override
    public <T> Optional<T> runInNewOptionalAwareTransaction(Function<DatabaseConnection, Optional<T>> callback, OnRollback onRollback) {
        return optionalAwareDatabaseAccessor.runInNewOptionalAwareTransaction(callback, onRollback);
    }

    @Override
    public <T> Optional<T> runInOptionalAwareTransaction(Function<DatabaseConnection, Optional<T>> callback, OnRollback onRollback) {
        return optionalAwareDatabaseAccessor.runInOptionalAwareTransaction(callback, onRollback);
    }

    @Override
    public <L, R> Either<L, R> runInNewEitherAwareTransaction(Function<DatabaseConnection, Either<L, R>> callback, OnRollback onRollback) {
        return eitherAwareDatabaseAccessor.runInNewEitherAwareTransaction(callback, onRollback);
    }

    @Override
    public <L, R> Either<L, R> runInEitherAwareTransaction(Function<DatabaseConnection, Either<L, R>> callback, OnRollback onRollback) {
        return eitherAwareDatabaseAccessor.runInEitherAwareTransaction(callback, onRollback);
    }

    private <T> T delegateCall(Connection connection, Function<DatabaseConnection, T> callback, boolean requireNew, OnRollback onRollback) {
        try {
            // this is the same logical side effect where the host would give out a connection
            executorFactory.setConnection(some(connection));
            //
            // we call the real PKQDSL code here to ensure that testing is production like
            if (requireNew) {
                return delegateDatabaseAccessor.runInNewTransaction(callback, onRollback);
            } else {
                return delegateDatabaseAccessor.runInTransaction(callback, onRollback);
            }
        } finally {
            executorFactory.setConnection(none());
        }
    }

    /**
     * Execute a <code>Function</code> with the pattern of:
     * <pre>
     * try (Connection cnn = connection()) {
     *     return function(cnn);
     * }
     * </pre>
     *
     * @param function the function to run.
     */
    public <T> T doWithConnection(final Function<Connection, T> function) {
        try (Connection connection = connection()) {
            return function.apply(connection);
        } catch (SQLException e) {
            throw new RuntimeException(
                    format("Error closing the connection:'%s' user='%s' password='%s'", url, user, password),
                    e
            );
        }
    }

    /**
     * Execute a <code>Consumer</code> with the pattern of:
     * <pre>
     * try (Connection cnn = connection()) {
     *     consume(cnn);
     * }
     * </pre>
     *
     * @param consumer the function to run.
     */
    public void doWithConnectionVoid(final Consumer<Connection> consumer) {
        try (Connection connection = connection()) {
            consumer.accept(connection);
        } catch (SQLException e) {
            throw new RuntimeException(
                    format("Error closing the connection:'%s' user='%s' password='%s'", url, user, password),
                    e
            );
        }
    }

    /**
     * Retreive the JDBC connection that will be used by this accessor.
     * <p>
     * Throws a runtime exception if the connection cannot be instantiated.
     *
     * @return valid connection
     */
    @Nonnull
    public Connection connection() {
        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            throw new RuntimeException(
                    format("unable to connect to database url:'%s' user='%s' password='%s'", url, user, password),
                    e
            );
        }
    }

    public String getUrl() {
        return url;
    }

    public String getSchema() {
        return schema;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

}
