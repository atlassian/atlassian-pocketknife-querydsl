package com.atlassian.pocketknife.test.util.querydsl;

import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Nonnull;
import org.junit.rules.ExternalResource;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.MultipleFailureException;
import org.junit.runners.model.Statement;

/**
 * Instantiate this class for use in standalone tests.
 * <p>
 * User specifies the database details via constructor parameters or system properties. If not specified, an in-memory
 * H2 database will be used.
 * <p>
 * {@link java.sql.DriverManager} provides the JDBC connections - it is up to the user to ensure that the correct JDBC
 * driver is present in the class loader.
 * <p>
 * User is responsible for creating and deleting test schema and data - whatever is currently present in the database
 * will persist.
 *
 * @deprecated migrate to JUnit 5 and use {@link StandaloneDatabaseAccessorExtension}
 */
@Deprecated
public class StandaloneDatabaseAccessor extends StandaloneDatabaseAccessorBase implements TestRule {

    /**
     * Constructs an accessor which uses connection information from system properties if present, otherwise in memory
     * H2.
     * <p>
     * Tests the connection once, throwing a runtime exception if it cannot be made.
     */
    public StandaloneDatabaseAccessor() {
        super();
    }

    /**
     * Constructs an accessor with user supplied connection info.
     * <p>
     * Tests the connection once, throwing a runtime exception if it cannot be made.
     *
     * @param url      must be resolvable by {@link DriverManager#getConnection(String, String, String)}
     * @param schema   may be empty
     * @param user     may be empty
     * @param password may be empty
     */
    public StandaloneDatabaseAccessor(@Nonnull final String url, @Nonnull final String schema, @Nonnull final String user, @Nonnull final String password) {
        super(url, schema, user, password);
    }

    /**
     * Shamelessly stolen from {@link ExternalResource}
     * @param base The {@link Statement} to be modified
     * @param description A {@link Description} of the test implemented in {@code base}
     * @return the modified {@link Statement} with @Before and @After support
     */
    @Override
    public Statement apply(Statement base, Description description) {
        return statement(base);
    }

    private Statement statement(final Statement base) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                before();

                List<Throwable> errors = new ArrayList<>();
                try {
                    base.evaluate();
                } catch (Throwable t) {
                    errors.add(t);
                } finally {
                    try {
                        after();
                    } catch (Throwable t) {
                        errors.add(t);
                    }
                }
                MultipleFailureException.assertEmpty(errors);
            }
        };
    }

    /**
     * Override to set up your specific external resource.
     *
     * @throws Throwable if setup fails (which will disable {@code after}
     */
    protected void before() throws Throwable {
        // do nothing
    }

    /**
     * Override to tear down your specific external resource.
     */
    protected void after() {
        // do nothing
    }
}
