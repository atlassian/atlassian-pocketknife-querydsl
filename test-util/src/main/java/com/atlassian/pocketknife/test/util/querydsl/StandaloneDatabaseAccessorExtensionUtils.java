package com.atlassian.pocketknife.test.util.querydsl;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import net.java.ao.RawEntity;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import static com.google.common.base.Strings.isNullOrEmpty;

/**
 * A utility class to assist the interaction with the database.
 */
public class StandaloneDatabaseAccessorExtensionUtils {

    /**
     * Sets up an AO database, as well as the tables for the given entities.
     * Executes SQL instructions provided by a a file, and then sets up the
     * database schema.
     * <p>
     * Use this method if there are tables that need to be created/populated
     * from a file in addition to tables that need to be created from AO entities.
     *
     * @param prefix      The prefix to be used by the database
     * @param inputStream The input stream for the resource containing the instructions to execute
     * @param entities    The list of entities to create tables for
     * @return The newly created StandaloneDatabaseAccessor
     */
    public static StandaloneDatabaseAccessorExtension setUpDatabaseUsingSQLFile(
            @Nonnull String prefix, @Nullable InputStream inputStream, @Nonnull final Class<? extends RawEntity<?>>... entities) throws Exception {
        if (inputStream == null) {
            return setUpDatabaseWithoutSQLFile(prefix, entities);
        }
        StandaloneAoDatabaseAccessorExtension databaseAccessor = new StandaloneAoDatabaseAccessorExtension(prefix, entities);
        executeSql(inputStream, databaseAccessor);
        return databaseAccessor;
    }

    /**
     * Sets up a database, executes SQL instructions provided by a file, and then
     * sets up the database schema.
     *
     * @param inputStream The input stream for the resource containing the instructions to execute
     * @return The newly created StandaloneDatabaseAccessor
     */
    public static StandaloneDatabaseAccessorExtension setUpDatabaseUsingSQLFile(@Nullable InputStream inputStream) throws Exception {
        if (inputStream == null) {
            return setUpDatabaseWithoutSQLFile();
        }
        StandaloneDatabaseAccessorExtension databaseAccessor = new StandaloneDatabaseAccessorExtension();
        executeSql(inputStream, databaseAccessor);
        return databaseAccessor;
    }

    /**
     * Sets up a database &amp; its schema.
     *
     * @return The newly created StandaloneDatabaseAccessor
     */
    public static StandaloneDatabaseAccessorExtension setUpDatabaseWithoutSQLFile() {
        return new StandaloneDatabaseAccessorExtension();
    }

    /**
     * Sets up an AO database, as well as the tables for the given entities,
     * and then initialises the schema provider.
     *
     * @param prefix   The prefix to be used by the database
     * @param entities The list of entities to create tables for
     * @return The newly created StandaloneDatabaseAccessor
     */
    public static StandaloneDatabaseAccessorExtension setUpDatabaseWithoutSQLFile(
            @Nonnull String prefix, @Nonnull final Class<? extends RawEntity<?>>... entities) {
        return new StandaloneAoDatabaseAccessorExtension(prefix, entities);
    }

    /**
     * Executes SQL instructions specified within the provided file using a Jdbc Template.
     *
     * @param inputStream      The input stream for the resource containing the instructions to execute
     * @param databaseAccessor The means to which the relevant database can be accessed
     */
    public static void executeSql(@Nonnull InputStream inputStream, @Nonnull StandaloneDatabaseAccessorExtension databaseAccessor) throws Exception {
        final JdbcOperations jdbcTemplate = new JdbcTemplate(new SingleConnectionDataSource(databaseAccessor.connection(), true));

        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream))) {
            String statement = bufferedReader.readLine();
            while (statement != null) {
                statement = statement.trim();
                if (!isNullOrEmpty(statement.trim())) {
                    jdbcTemplate.execute(statement);
                }
                statement = bufferedReader.readLine();
            }
        } finally {
            databaseAccessor.connection().commit();
        }
    }

    /**
     * Attempts to drop the table with the given name.
     * Failure to do so will invoke attempts to drop the table with alternative
     * naming/casing approaches.
     *
     * @param tableName        The name of the table to attempt to drop
     * @param databaseAccessor The means to which the relevant database can be accessed
     */
    public static void dropTable(final String tableName, final StandaloneDatabaseAccessorExtension databaseAccessor) {
        // Execute quietly because the table may not exist
        executeQuietly("drop table " + tableName, databaseAccessor);
        executeQuietly("drop table \"" + tableName + "\"", databaseAccessor);
        // Execute quietly because this only applies to Oracle
        executeQuietly("drop sequence \"" + tableName + "_ID_SEQ\"", databaseAccessor);
    }

    /**
     * Executes the given SQL, ignoring any {@link SQLException}s.
     *
     * @param sql              the SQL to execute
     * @param databaseAccessor the db accessor to use
     */
    private static void executeQuietly(final String sql, final StandaloneDatabaseAccessorExtension databaseAccessor) {
        try {
            databaseAccessor.connection().prepareStatement(sql).execute();
        } catch (final SQLException e) {
            // Ignore
        } catch (final RuntimeException e) {
            if (!wasCausedBy(e, SQLException.class)) {
                throw e;
            }
        }
    }

    /**
     * Indicates whether the given throwable is (or was caused by) a throwable of the given type.
     *
     * @param t              the throwable to check
     * @param throwableClass the type of throwable to check for
     * @return see above
     */
    private static boolean wasCausedBy(Throwable t, final Class<? extends Throwable> throwableClass) {
        while (t != null) {
            if (throwableClass.isInstance(t)) {
                return true;
            }
            t = t.getCause();
        }

        return false;
    }
}


