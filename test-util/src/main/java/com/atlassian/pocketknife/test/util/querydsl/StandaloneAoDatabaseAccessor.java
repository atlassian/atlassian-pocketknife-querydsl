package com.atlassian.pocketknife.test.util.querydsl;

import javax.annotation.Nonnull;
import net.java.ao.RawEntity;

/**
 * Extension of {@link StandaloneDatabaseAccessor} which allows creation of many ActiveObjects {@link RawEntity}.
 * <p>
 * ActiveObjects entities will be created on first invocation. As per parent, the user is responsible for clearing any
 * database objects or data on subsequent invocations.
 *
 * @deprecated migrate to JUnit 5 and use {@link StandaloneAoDatabaseAccessorExtension}
 */
@Deprecated
@SuppressWarnings({"unchecked", "SqlNoDataSourceInspection"})
public class StandaloneAoDatabaseAccessor extends StandaloneDatabaseAccessor {
    private final StandaloneAoDatabaseAccessorBase aoBase;

    /**
     * {@inheritDoc}
     *
     * @param entities ActiveObjects entities to create if not present
     */
    public StandaloneAoDatabaseAccessor(@Nonnull final String prefix, final Class<? extends RawEntity<?>>... entities) {
        super();
        this.aoBase = new StandaloneAoDatabaseAccessorBase(this, prefix, entities);
    }

    /**
     * {@inheritDoc}
     *
     * @param entities ActiveObjects entities to create if not present
     */
    public StandaloneAoDatabaseAccessor(@Nonnull final String prefix,
                                        @Nonnull final String schema,
                                        @Nonnull String url,
                                        @Nonnull String user,
                                        @Nonnull String password,
                                        final Class<? extends RawEntity<?>>... entities) {
        super(url, schema, user, password);
        this.aoBase = new StandaloneAoDatabaseAccessorBase(this, prefix, entities);
    }

    @Override
    protected void before() throws Throwable {
        super.before();

        // Drop and re-create everything.
        aoBase.before();
    }

    @Override
    protected void after() {
        aoBase.after();
    }

    /**
     * Drop a table, use in case you don't like to use StandaloneAoDatabaseAccessor as a @Rule
     *
     * @param tableName table name to drop.
     */
    public void dropTable(String tableName) {
        aoBase.dropTable(tableName);
    }
}
