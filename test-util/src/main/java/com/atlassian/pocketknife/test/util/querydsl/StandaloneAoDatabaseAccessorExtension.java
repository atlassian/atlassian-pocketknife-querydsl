package com.atlassian.pocketknife.test.util.querydsl;

import java.sql.DriverManager;
import javax.annotation.Nonnull;
import net.java.ao.RawEntity;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

/**
 * Instantiate this class for use in standalone tests.
 * <p>
 * User specifies the database details via constructor parameters or system properties. If not specified, an in-memory
 * H2 database will be used.
 * <p>
 * {@link DriverManager} provides the JDBC connections - it is up to the user to ensure that the correct JDBC
 * driver is present in the class loader.
 * <p>
 * User is responsible for creating and deleting test schema and data - whatever is currently present in the database
 * will persist.
 */
public class StandaloneAoDatabaseAccessorExtension extends StandaloneDatabaseAccessorExtension implements BeforeEachCallback, AfterEachCallback {
    private final StandaloneAoDatabaseAccessorBase aoBase;

    /**
     * {@inheritDoc}
     *
     * @param entities ActiveObjects entities to create if not present
     */
    public StandaloneAoDatabaseAccessorExtension(@Nonnull final String prefix, final Class<? extends RawEntity<?>>... entities) {
        super();
        this.aoBase = new StandaloneAoDatabaseAccessorBase(this, prefix, entities);
    }

    /**
     * {@inheritDoc}
     *
     * @param entities ActiveObjects entities to create if not present
     */
    public StandaloneAoDatabaseAccessorExtension(@Nonnull final String prefix, @Nonnull final String schema, @Nonnull String url, @Nonnull String user, @Nonnull String password, final Class<? extends RawEntity<?>>... entities) {
        super(url, schema, user, password);
        this.aoBase = new StandaloneAoDatabaseAccessorBase(this, prefix, entities);
    }

    @Override
    public void beforeEach(ExtensionContext extensionContext) {
        super.beforeEach(extensionContext);
        aoBase.before();
    }

    @Override
    public void afterEach(ExtensionContext extensionContext) {
        super.afterEach(extensionContext);
        aoBase.after();
    }
    /**
     * Drop a table, use in case you don't like to use StandaloneAoDatabaseAccessor as a @Rule
     *
     * @param tableName table name to drop.
     */
    public void dropTable(String tableName) {
        aoBase.dropTable(tableName);
    }
}
