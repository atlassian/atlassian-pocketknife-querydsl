package com.atlassian.pocketknife.test.util.querydsl;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.assertThrows;
import static uk.org.webcompere.systemstubs.SystemStubs.restoreSystemProperties;

public class StandaloneDatabaseAccessorSystemPropertiesTest {
    @Test
    public void testCustomProperties() throws Exception {
        restoreSystemProperties(() -> {
            System.setProperty(StandaloneDatabaseAccessor.SYS_PROP_URL, "someUrl");
            System.setProperty(StandaloneDatabaseAccessor.SYS_PROP_USER, "someUser");
            System.setProperty(StandaloneDatabaseAccessor.SYS_PROP_PASSWORD, "somePassword");

            final RuntimeException e = assertThrows(RuntimeException.class, StandaloneDatabaseAccessor::new);
            assertThat(e.getMessage(), containsString("unable to connect"));
            assertThat(e.getMessage(), containsString("someUrl"));
            assertThat(e.getMessage(), containsString("someUser"));
            assertThat(e.getMessage(), containsString("somePassword"));
        });
    }
}
