package com.atlassian.pocketknife.test.util.querydsl;

import io.atlassian.fugue.Either;
import io.atlassian.fugue.Unit;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

import static com.atlassian.pocketknife.api.querydsl.util.OnRollback.NOOP;
import static io.atlassian.fugue.Either.left;
import static io.atlassian.fugue.Either.right;
import static io.atlassian.fugue.Unit.Unit;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.fail;

class StandaloneAoDatabaseAccessorExtensionQueryDslTest {
    @RegisterExtension
    static StandaloneDatabaseAccessorExtension databaseAccessor = new StandaloneAoDatabaseAccessorExtension("AO_012345", AOSomeTable.class);

    @Test
    void testRunInsertSelect() {
        final QSomeTable someTable = new QSomeTable();

        databaseAccessor.runInNewTransaction(databaseConnection -> {
            databaseConnection
                .insert(someTable)
                .columns(someTable.val)
                .values("val1")
                .execute();
            return null;
        }, NOOP);

        databaseAccessor.runInNewTransaction(databaseConnection -> {
            final List<String> vals = databaseConnection
                .select(someTable.val)
                .from(someTable)
                .where(someTable.val.eq("val1"))
                .limit(1)
                .fetch();

            assertThat("Should return 1 result", vals.size(), is(1));
            assertThat(vals.get(0), is("val1"));
            return null;
        }, NOOP);
    }

    @Test
    void testRunInsertSelectOptionalAware() {
        final QSomeTable someTable = new QSomeTable();

        databaseAccessor.runInNewOptionalAwareTransaction(databaseConnection -> {
            databaseConnection
                .insert(someTable)
                .columns(someTable.val)
                .values("val1")
                .execute();
            return Optional.of(Unit());
        }, NOOP);

        databaseAccessor.runInNewOptionalAwareTransaction(databaseConnection -> {
            final List<String> vals = databaseConnection
                .select(someTable.val)
                .from(someTable)
                .where(someTable.val.eq("val1"))
                .limit(1)
                .fetch();

            assertThat("Should return 1 result", vals.size(), is(1));
            assertThat(vals.get(0), is("val1"));
            return Optional.of(Unit());
        }, NOOP);
    }

    @Test
    void testRunInsertSelectEitherAware() {
        final QSomeTable someTable = new QSomeTable();

        databaseAccessor.runInNewEitherAwareTransaction(databaseConnection -> {
            databaseConnection
                .insert(someTable)
                .columns(someTable.val)
                .values("val1")
                .execute();
            return right(Unit());
        }, NOOP);

        databaseAccessor.runInNewEitherAwareTransaction(databaseConnection -> {
            final List<String> vals = databaseConnection
                .select(someTable.val)
                .from(someTable)
                .where(someTable.val.eq("val1"))
                .limit(1)
                .fetch();

            assertThat("Should return 1 result", vals.size(), is(1));
            assertThat(vals.get(0), is("val1"));
            return right(Unit());
        }, NOOP);
    }

    @Test
    void testRunInsertDelete() {
        final QSomeTable someTable = new QSomeTable();
        final AtomicBoolean onRollback = new AtomicBoolean(false);

        databaseAccessor.runInNewTransaction(databaseConnection -> {
            databaseConnection
                .insert(someTable)
                .columns(someTable.val)
                .values("val1")
                .execute();
            return null;
        }, () -> onRollback.set(true));
        assertThat("onRollback should not be executed", onRollback.get(), is(false));

        databaseAccessor.runInNewTransaction(databaseConnection -> {
            databaseConnection
                .delete(someTable)
                .where(someTable.val.eq("val1"))
                .execute();
            return null;
        }, NOOP);

        databaseAccessor.runInNewTransaction(databaseConnection -> {
            final List<String> vals = databaseConnection
                .select(someTable.val)
                .from(someTable)
                .where(someTable.val.eq("val1"))
                .limit(1)
                .fetch();

            assertThat("Should return no results", vals, empty());
            return null;
        }, NOOP);
    }

    @Test
    void testRunInsertDeleteOptionalAware() {
        final QSomeTable someTable = new QSomeTable();
        final AtomicBoolean onRollback = new AtomicBoolean(false);

        final Optional<Unit> result =
            databaseAccessor.runInNewOptionalAwareTransaction(databaseConnection -> {
                databaseConnection
                    .insert(someTable)
                    .columns(someTable.val)
                    .values("val1")
                    .execute();
                return Optional.of(Unit());
            }, () -> onRollback.set(true));
        assertThat("The of is expected to be returned", result, is(Optional.of(Unit())));
        assertThat("onRollback should not be executed", onRollback.get(), is(false));

        databaseAccessor.runInNewOptionalAwareTransaction(databaseConnection -> {
            databaseConnection
                .delete(someTable)
                .where(someTable.val.eq("val1"))
                .execute();
            return Optional.of(Unit());
        }, NOOP);

        databaseAccessor.runInNewOptionalAwareTransaction(databaseConnection -> {
            final List<String> vals = databaseConnection
                .select(someTable.val)
                .from(someTable)
                .where(someTable.val.eq("val1"))
                .limit(1)
                .fetch();

            assertThat("Should return no results", vals, empty());
            return Optional.of(Unit());
        }, NOOP);
    }

    @Test
    void testRunInsertDeleteEitherAware() {
        final QSomeTable someTable = new QSomeTable();
        final AtomicBoolean onRollback = new AtomicBoolean(false);

        final Either<Object, Unit> result =
            databaseAccessor.runInNewEitherAwareTransaction(databaseConnection -> {
                databaseConnection
                    .insert(someTable)
                    .columns(someTable.val)
                    .values("val1")
                    .execute();
                return right(Unit());
            }, () -> onRollback.set(true));
        assertThat("The right is expected to be returned", result, is(right(Unit())));
        assertThat("onRollback should not be executed", onRollback.get(), is(false));

        databaseAccessor.runInNewEitherAwareTransaction(databaseConnection -> {
            databaseConnection
                .delete(someTable)
                .where(someTable.val.eq("val1"))
                .execute();
            return right(Unit());
        }, NOOP);

        databaseAccessor.runInNewEitherAwareTransaction(databaseConnection -> {
            final List<String> vals = databaseConnection
                .select(someTable.val)
                .from(someTable)
                .where(someTable.val.eq("val1"))
                .limit(1)
                .fetch();

            assertThat("Should return no results", vals, empty());
            return right(Unit());
        }, NOOP);
    }

    @Test
    void testRunInsertRollback() {
        final QSomeTable someTable = new QSomeTable();
        final AtomicBoolean onRollback = new AtomicBoolean(false);

        try {
            databaseAccessor.runInNewTransaction(databaseConnection -> {
                databaseConnection
                    .insert(someTable)
                    .columns(someTable.val)
                    .values("val1")
                    .execute();
                throw new RuntimeException("It failed");
            }, () -> onRollback.set(true));
            fail("Expected exception to be rethrown");
        } catch (RuntimeException re) {
            assertThat(re.getMessage(), is("It failed"));
        }
        assertThat("OnRollback expected to be executed", onRollback.get(), is(true));

        databaseAccessor.runInNewTransaction(databaseConnection -> {
            final List<String> vals = databaseConnection
                .select(someTable.val)
                .from(someTable)
                .where(someTable.val.eq("val1"))
                .limit(1)
                .fetch();

            assertThat("Should return no results", vals, empty());
            return null;
        }, NOOP);
    }

    @Test
    void testRunInsertRollbackOptionalAware() {
        final QSomeTable someTable = new QSomeTable();
        final AtomicBoolean onRollback = new AtomicBoolean(false);

        final Optional<Object> result =
            databaseAccessor.runInNewOptionalAwareTransaction(databaseConnection -> {
                databaseConnection
                    .insert(someTable)
                    .columns(someTable.val)
                    .values("val1")
                    .execute();
                // rollback on empty
                return Optional.empty();
            }, () -> onRollback.set(true));
        assertThat("The empty is expected to be returned", result, is(Optional.empty()));
        assertThat("OnRollback expected to be executed", onRollback.get(), is(true));

        databaseAccessor.runInNewTransaction(databaseConnection -> {
            final List<String> vals = databaseConnection
                .select(someTable.val)
                .from(someTable)
                .where(someTable.val.eq("val1"))
                .limit(1)
                .fetch();

            assertThat("Should return no results", vals, empty());
            return null;
        }, NOOP);
    }

    @Test
    void testRunInsertRollbackEitherAware() {
        final QSomeTable someTable = new QSomeTable();
        final AtomicBoolean onRollback = new AtomicBoolean(false);

        final Either<Unit, Object> result =
            databaseAccessor.runInNewEitherAwareTransaction(databaseConnection -> {
                databaseConnection
                    .insert(someTable)
                    .columns(someTable.val)
                    .values("val1")
                    .execute();
                // rollback on left
                return left(Unit());
            }, () -> onRollback.set(true));
        assertThat("The left is expected to be returned", result, is(left(Unit())));
        assertThat("OnRollback expected to be executed", onRollback.get(), is(true));

        databaseAccessor.runInNewTransaction(databaseConnection -> {
            final List<String> vals = databaseConnection
                .select(someTable.val)
                .from(someTable)
                .where(someTable.val.eq("val1"))
                .limit(1)
                .fetch();

            assertThat("Should return no results", vals, empty());
            return null;
        }, NOOP);
    }
}
