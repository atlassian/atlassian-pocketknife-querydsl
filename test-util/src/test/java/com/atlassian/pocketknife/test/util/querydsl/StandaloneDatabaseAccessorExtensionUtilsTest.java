package com.atlassian.pocketknife.test.util.querydsl;

import org.junit.Test;

public class StandaloneDatabaseAccessorExtensionUtilsTest {
    @Test
    public void shouldNotThrowWhenDroppingNonExistentTable() {
        // Invoke
        StandaloneDatabaseAccessorExtensionUtils.dropTable("nosuchtable", new StandaloneDatabaseAccessorExtension());
    }
}