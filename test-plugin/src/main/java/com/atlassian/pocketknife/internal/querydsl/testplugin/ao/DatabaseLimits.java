package com.atlassian.pocketknife.internal.querydsl.testplugin.ao;

public final class DatabaseLimits
{
    private DatabaseLimits()
    {
        throw new Error("This class is static only");
    }

    public static final int UNLIMITED_RUFLINS = -1;
    public static final int MAX_RUFLINS_450 = 450;
    public static final int FOUR_RUFLINS_255 = 255;
    public static final int TWO_RUFLINS_127 = 127;
    public static final int ONE_RUFLIN_63 = 63;
}
