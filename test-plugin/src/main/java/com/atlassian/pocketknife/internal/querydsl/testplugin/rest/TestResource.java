package com.atlassian.pocketknife.internal.querydsl.testplugin.rest;

import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;
import com.atlassian.pocketknife.api.querydsl.DatabaseAccessor;
import com.atlassian.pocketknife.api.querydsl.schema.SchemaState;
import com.atlassian.pocketknife.api.querydsl.schema.SchemaStateProvider;
import com.atlassian.pocketknife.api.querydsl.stream.CloseableIterable;
import com.atlassian.pocketknife.api.querydsl.stream.StreamingQueryFactory;
import com.atlassian.pocketknife.internal.querydsl.testplugin.tables.QCustomerTable;
import com.atlassian.pocketknife.internal.querydsl.testplugin.tables.Tables;
import com.atlassian.pocketknife.internal.querydsl.util.Unit;
import com.querydsl.core.Tuple;
import java.io.PrintWriter;
import java.util.Date;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import javax.ws.rs.core.UriInfo;

import static com.atlassian.pocketknife.api.querydsl.util.OnRollback.NOOP;
import static com.atlassian.pocketknife.internal.querydsl.testplugin.tables.Tables.CUSTOMER;
import static com.atlassian.pocketknife.internal.querydsl.testplugin.tables.Tables.ISSUE;

@UnrestrictedAccess
@Path("test")
public class TestResource {
    private final DatabaseAccessor databaseAccessor;
    private final StreamingQueryFactory streamingQueryFactory;
    private final SchemaStateProvider schemaStateProvider;

    @Inject
    public TestResource(final DatabaseAccessor databaseAccessor,
                        final StreamingQueryFactory queryFactory,
                        final SchemaStateProvider schemaStateProvider) {
        this.databaseAccessor = databaseAccessor;
        this.streamingQueryFactory = queryFactory;
        this.schemaStateProvider = schemaStateProvider;
    }

    @SuppressWarnings("unused")
    @GET
    public Response read(@Context UriInfo uriInfo) {
        // just to prove we can hit EnhancedRelationPaths outside a querty context in PKQDSL 4.x
        // here is an example that used to blow up
        QCustomerTable customerTable = Tables.CUSTOMER;

        // now lets stream some data back
        StreamingOutput jacksonStream = output -> {
            PrintWriter out = new PrintWriter(output);
            out.println("Reading SQL data @" + new Date());
            readData(out);
        };
        return Response.ok(jacksonStream).type(MediaType.TEXT_PLAIN_TYPE).build();
    }

    private void readData(PrintWriter out) {
        databaseAccessor.runInNewTransaction(dbConn -> {
            try (CloseableIterable<Tuple> result = streamingQueryFactory.stream(dbConn, () -> dbConn.select(CUSTOMER.all()).from(CUSTOMER))) {
                for (Tuple tuple : result) {
                    out.println(tuple);
                }
            }

            out.println("Trying to access the JIRA table 'jiraissue'");
            try (CloseableIterable<Tuple> result = streamingQueryFactory.stream(dbConn, () -> dbConn.select(ISSUE.all()).from(ISSUE))) {
                for (Tuple tuple : result) {
                    out.println(tuple);
                }
            } catch (RuntimeException rte) {
                out.println("Whoa that didn't work");
                rte.printStackTrace(out);
            }

            out.println("Done");

            out.println("Accessing schema state");
            SchemaState schemaState = schemaStateProvider.getSchemaState(dbConn.getJdbcConnection(), CUSTOMER);
            out.println("Got schema state :" + schemaState);

            return Unit.VALUE;
        }, NOOP);
        out.flush();
    }
}
