package com.atlassian.pocketknife.internal.querydsl.testplugin.bootstrap;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.google.common.annotations.VisibleForTesting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * The Launcher is the starting point for the plugin.
 *
 * This should be the only life cycle listener in the system and it should then delegate out to other code as
 * appropriate.
 *
 * Its purpose is to allow us to reason about where events enter the system and the order of called code.  Having a
 * single listener / entry point object allows us to do that.
 */
@ExportAsService
@Component
public class Launcher implements LifecycleAware {
    private static final Logger log = LoggerFactory.getLogger(Launcher.class);
    private static final String PKQDSL_TEST_PLUGIN = "Pocketknife QueryDSL test-plugin";

    private final static String PLUGIN_STARTED_EVENT = "plugin.started.event";
    private final static String LIFECYCLE_AWARE_ONSTART = "lifecycle.aware.onstart";
    private final static String PLUGIN_KEY = "com.atlassian.pocketknife.querydsl.testplugin";

    public Launcher() {
        //
        // we want our logs to go out as info even if the host defaults to warn
        LogLeveller.setInfo(log);
    }


    @Override
    public void onStart() {
        onStartCompleted();
    }

    @Override
    public void onStop() {
    }

    @PostConstruct
    public void onSpringContextStarted() {
        log.info(PKQDSL_TEST_PLUGIN + " spring context is starting...");
    }

    @VisibleForTesting
    void onStartCompleted() {
        log.info(PKQDSL_TEST_PLUGIN + " is initializing...");

        // the pkqdsl priming should handle our AO coming up
        log.info("Go to the following to run some SQL \n\n\thttp://localhost:2990/jira/rest/pkqdsl/1.0/test\n");

        log.info(PKQDSL_TEST_PLUGIN + " is initialized.");

    }

    @PreDestroy
    public void onSpringContextStopped() {
        log.info(PKQDSL_TEST_PLUGIN + " spring context is stopping...");

        log.info(PKQDSL_TEST_PLUGIN + " spring context is stopped.");
    }
}
