package com.atlassian.pocketknife.internal.querydsl.testplugin.tables;

import com.atlassian.pocketknife.spi.querydsl.EnhancedRelationalPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;

/**
 */
public class QAddressTable extends EnhancedRelationalPathBase<QAddressTable> {
    // Columns
    public final NumberPath<Long> ID = createLongCol("ID").asPrimaryKey().build();
    public final StringPath ADDRESS_TYPE = createStringCol("ADDRESS_TYPE").notNull().build();
    public final StringPath STREET_NAME1 = createStringCol("STREET_NAME1").notNull().build();
    public final StringPath STREET_NAME2 = createStringCol("STREET_NAME2").notNull().build();
    public final StringPath SUBURB = createStringCol("SUBURB").notNull().build();
    public final StringPath POST_CODE = createStringCol("POST_CODE").notNull().build();


    public QAddressTable(final String tableName) {
        super(QAddressTable.class, tableName);
    }

}
