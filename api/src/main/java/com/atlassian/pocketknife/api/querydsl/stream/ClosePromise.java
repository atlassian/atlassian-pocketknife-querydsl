package com.atlassian.pocketknife.api.querydsl.stream;

import javax.annotation.concurrent.NotThreadSafe;
import java.io.Closeable;

import static java.util.Objects.requireNonNull;

/**
 * A ClosePromise will call the effect once and only once when it is closed and once its is closed, it is considered
 * closed forever.
 *
 * This promise is not thread safe.
 */
@NotThreadSafe
public class ClosePromise implements Closeable
{
    private static final Runnable NOOP_RUNNABLE = () -> {
    };
    /**
     * A close promise that does nothing. Since Close Promises are mutable you need a new one every time
     */
    public static ClosePromise NOOP()
    {
        return new ClosePromise();
    }

    private boolean closed = false;
    private final ClosePromise parentPromise;
    private final Runnable closeEffect;

    private ClosePromise()
    {
        this(null, NOOP_RUNNABLE);
    }

    /**
     * A promise to do something when this promise is closed
     *
     * @param closeEffect what happens when the promise is closed
     */
    public ClosePromise(Runnable closeEffect)
    {
        this(NOOP(), closeEffect);
    }

    /**
     * You can chain together promises such that the child is closed first then the parent
     *
     * @param parentPromise the parent to close after yourself
     * @param closeEffect what happens when the promise is closed
     */
    public ClosePromise(ClosePromise parentPromise, Runnable closeEffect)
    {
        this.parentPromise = parentPromise;
        this.closeEffect = requireNonNull(closeEffect);
    }


    @Override
    public void close()
    {
        if (!closed)
        {
            closed = true;
            try
            {
                closeEffect.run();
            }
            finally
            {
                if (parentPromise != null)
                {
                    parentPromise.close();
                }
            }
        }
    }

    public boolean isClosed()
    {
        return closed;
    }
}
