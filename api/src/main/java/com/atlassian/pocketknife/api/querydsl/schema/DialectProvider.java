package com.atlassian.pocketknife.api.querydsl.schema;

import com.atlassian.annotations.PublicApi;
import com.querydsl.sql.Configuration;
import com.querydsl.sql.SQLTemplates;

import java.sql.Connection;

/**
 * This provides dialect configuration so QueryDSL can perform
 * its cross platform database magic.  This must be detected at runtime
 * since we cant know what database we are on until then.
 */
@PublicApi
public interface DialectProvider
{
    /**
     * Called to get the configuration of the database.  This should probably be cached because it will be called for
     * EVERY QueryDSL call made.  Its up to the implementation but it might be expensive to calculate
     *
     * @param connection the connection in play
     * @return a configuration of the underlying database
     */
    Config getDialectConfig(Connection connection);

    /**
     * These are the database products that Pocketknife knows about.  If we add new supported databases, then we MUST add a new enum and QueryDSL support
     */
    enum SupportedDatabase
    {
        POSTGRESSQL, ORACLE, MYSQL, SQLSERVER, HSQLDB, H2
    }

    /**
     * Information about the current database
     */
    class DatabaseInfo {
        private final String databaseProductName;
        private final String databaseProductVersion;
        private final String driverName;
        private final int databaseMajorVersion;
        private final int databaseMinorVersion;
        private final int driverMajorVersion;
        private final int driverMinorVersion;
        private final SupportedDatabase supportedDatabase;

        public DatabaseInfo(final SupportedDatabase supportedDatabase, final String databaseProductName, final String databaseProductVersion, final int databaseMajorVersion, final int databaseMinorVersion, final String driverName, final int driverMajorVersion, final int driverMinorVersion)
        {
            this.supportedDatabase = supportedDatabase;
            this.databaseProductName = databaseProductName;
            this.databaseProductVersion = databaseProductVersion;
            this.databaseMajorVersion = databaseMajorVersion;
            this.databaseMinorVersion = databaseMinorVersion;
            this.driverName = driverName;
            this.driverMajorVersion = driverMajorVersion;
            this.driverMinorVersion = driverMinorVersion;
        }

        public SupportedDatabase getSupportedDatabase()
        {
            return supportedDatabase;
        }

        public String getDatabaseProductName()
        {
            return databaseProductName;
        }

        public String getDatabaseProductVersion()
        {
            return databaseProductVersion;
        }

        public String getDriverName()
        {
            return driverName;
        }

        public int getDatabaseMajorVersion()
        {
            return databaseMajorVersion;
        }

        public int getDatabaseMinorVersion()
        {
            return databaseMinorVersion;
        }

        public int getDriverMajorVersion()
        {
            return driverMajorVersion;
        }

        public int getDriverMinorVersion()
        {
            return driverMinorVersion;
        }

    }

    class Config
    {
        private final SQLTemplates sqlTemplates;
        private final Configuration configuration;
        private final DatabaseInfo databaseInfo;

        public Config(final SQLTemplates sqlTemplates, final Configuration configuration, final DatabaseInfo databaseInfo)
        {
            this.sqlTemplates = sqlTemplates;
            this.configuration = configuration;
            this.databaseInfo = databaseInfo;
        }

        public SQLTemplates getSqlTemplates()
        {
            return sqlTemplates;
        }

        public Configuration getConfiguration()
        {
            return configuration;
        }

        public DatabaseInfo getDatabaseInfo()
        {
            return databaseInfo;
        }
    }

}
