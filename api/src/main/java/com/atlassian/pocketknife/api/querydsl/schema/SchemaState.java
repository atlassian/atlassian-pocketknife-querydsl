package com.atlassian.pocketknife.api.querydsl.schema;

import com.atlassian.annotations.PublicApi;
import com.querydsl.core.types.Path;
import com.querydsl.sql.RelationalPath;

import java.util.Set;

/**
 * This gives back information about the state of the schema given a {@link com.querydsl.sql.RelationalPath}
 * allowing you to do 'fast-five' style code techniques.
 *
 * @since v4.0
 */
@PublicApi
public interface SchemaState {

    /**
     * @return the relational path this state is for
     */
    RelationalPath getRelationalPath();

    /**
     * @return whether the table is the same, different or missing
     */
    Presence getTableState();

    /**
     * @param column the column to check
     *
     * @return whether the column is the same, different or missing
     */
    Presence getColumnState(Path<?> column);

    /**
     * @return the list of missing columns as compared to the RelationalPath definition
     */
    Set<Path<?>> getMissingColumns();

    /**
     * @return the list of added columns as compared to the RelationalPath definition
     */
    Set<String> getAddedColumns();

    enum Presence {
        /**
         * The table or column is the same as defined in the RelationalPath
         */
        SAME,
        /**
         * The table or column is different to how it is defined in the RelationalPath
         */
        DIFFERENT,
        /**
         * The table or column is missing compared to the RelationalPath
         */
        MISSING
    }

}
