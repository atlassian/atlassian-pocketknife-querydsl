package com.atlassian.pocketknife.internal.querydsl.cache;

public interface PKQCacheClearer {
    void onClearCache(Object event);

    void registerCacheClearing(Runnable runnable);

    void clearAllCaches();
}
