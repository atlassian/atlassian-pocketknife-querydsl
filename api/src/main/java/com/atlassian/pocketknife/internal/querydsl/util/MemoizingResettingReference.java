package com.atlassian.pocketknife.internal.querydsl.util;

import javax.annotation.concurrent.ThreadSafe;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;
import java.util.function.Supplier;

import static java.util.Objects.requireNonNull;

/**
 * This is a reference object that is
 * <pre>
 *     * thread safe
 *     * memoizing
 *     * guaranteed once only construction
 *     * lazy access
 *     * prevents poisoned cached values
 * </pre>
 *
 * If an exception is throw during the value creation then the reference is not cached and reset
 *
 * @param <T> The type of object referred to by this reference
 * @param <P> The type of parameter needed to get values of T
 */
@ThreadSafe
public class MemoizingResettingReference<P, T>
{
    private final AtomicReference<Supplier<T>> supplierRef;
    private final Function<P, T> valueCreator;

    /**
     * The reason the function returns a Supplier of T is so we can have lazy and once only semantics if 2 threads
     * race to create the underlying value
     *
     * @param valueCreator a T
     */
    public MemoizingResettingReference(Function<P, T> valueCreator)
    {
        this.supplierRef = new AtomicReference<>();
        this.valueCreator = requireNonNull(valueCreator);
    }

    /**
     * Gets the reference and passes in as parameter P so if can lazily create a the T if need be
     *
     * @param parameter the parameter needed to create the T object
     * @return a T object
     */
    public T get(final P parameter)
    {
        Supplier<T> supplier = supplierRef.get();
        if (supplier == null)
        {
            // using a supplier allows us to be lazy and create 2 suppliers
            // but only have 1 guaranteed thread safe call to the creator function
            Supplier<T> delegate = () -> valueCreator.apply(parameter);
            supplierRef.compareAndSet(null, new SmarterMemoizingSupplier<>(delegate));
        }
        supplier = supplierRef.get();
        return safelyGetT(supplier);
    }

    private T safelyGetT(final Supplier<T> supplier)
    {
        try
        {
            // by calling get - we apply the function and create the actual value.  But only once because of the memoize()
            return requireNonNull(supplier.get(),"You MUST not provide null values to MemoizingResettingReference");
        }
        catch (RuntimeException rte)
        {
            reset();
            throw rte;
        }
    }

    public static class MemoizedValueNotPresentException extends RuntimeException
    {
        public MemoizedValueNotPresentException()
        {
            super("MemoizingResettingReference.getMemoizedValue called and the value is not previously been initialised via MemoizingResettingReference.get(<P>)");
        }
    }

    /**
     * This will return the memoized value with the assumption that its already cached via a called to {@link #get(Object)}.  if that has not happened
     * then it will throw an illegal state exception.
     *
     * @return the memoized value previously cached via a call to {@link #get(Object)}
     * @throws MemoizedValueNotPresentException if the value has not be previously memoized
     */
    public T getMemoizedValue() throws MemoizedValueNotPresentException
    {
        Supplier<T> supplier = supplierRef.get();
        if (supplier == null)
        {
            throw new MemoizedValueNotPresentException();
        }
        return safelyGetT(supplier);
    }

    /**
     * You can reset the reference if need be
     */
    public void reset()
    {
        supplierRef.set(null);
    }

    /**
     * The reason for this wrapper versus Guava Suppliers.memoize() is that without it we have GC problems.
     * <p>
     * Let's have cfuller explain "...[Guava Suppliers.memoize] is not 100% GC-clean. If we "win" and our Supplier is the one that
     * gets used/kept, then 'parameter' will never be GC'd because the Supplier still references it. :(
     *   * The MemoizingSupplier will only call its delegate once, but the delegate does not know this, and the
     * MemoizingSupplier does not throw away the delegate when it is done with it. Honestly, I would consider this to be
     * a design flaw in Suppliers.memoize, which could have thrown away its delegate pointer after initializing, but
     * life^H^H^H^HGoogle gave us lemons, here. "
     * <p>
     * So we wrap it into a re-implementation of Guava supplier delegate that clears its reference to the delegate after construction
     * and hence any precious parameters (like connections) will be GC cleared after first time use
     * <p>
     * TODO: verify if this class can be replaced with {@link io.atlassian.fugue.Suppliers#memoize}
     */
    private static class SmarterMemoizingSupplier<T> implements Supplier<T>
    {
        private final AtomicReference<Supplier<T>> delegate;
        private final AtomicReference<T> value;
        private final AtomicBoolean initialized;

        private SmarterMemoizingSupplier(Supplier<T> delegate)
        {
            this.initialized = new AtomicBoolean(false);
            this.delegate = new AtomicReference<>(delegate);
            this.value = new AtomicReference<>();
        }

        @Override
        public T get()
        {
            if (!initialized.get())
            {
                synchronized (this)
                {
                    if (!initialized.get())
                    {
                        T t = delegate.get().get();
                        value.set(t);
                        delegate.set(null);// <-- secret sauce of win
                        initialized.set(true);
                    }
                }
            }
            return this.value.get();
        }
    }
}
