package com.atlassian.pocketknife.internal.querydsl.schema;

import com.atlassian.pocketknife.internal.querydsl.cache.PKQCacheClearer;
import com.google.common.annotations.VisibleForTesting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.PostConstruct;
import java.sql.Connection;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import static com.google.common.base.Preconditions.checkArgument;
import static java.util.Objects.requireNonNull;

@Component
@ParametersAreNonnullByDefault
public class DefaultSchemaProvider implements SchemaProvider {

    private static final Logger log = LoggerFactory.getLogger(DefaultSchemaProvider.class);

    private final ConcurrentHashMap<UpperCaseNameKey, String> tableAndColumnNames;

    private final ProductSchemaProvider productSchemaProvider;
    private final PKQCacheClearer cacheClearer;
    private final JdbcTableInspector tableInspector;

    @Autowired
    public DefaultSchemaProvider(final ProductSchemaProvider productSchemaProvider, JdbcTableInspector tableInspector, PKQCacheClearer cacheClearer) {
        this.productSchemaProvider = productSchemaProvider;
        this.tableInspector = tableInspector;
        this.cacheClearer = cacheClearer;
        this.tableAndColumnNames = new ConcurrentHashMap<>();
    }

    @PostConstruct
    void postConstruct() {
        cacheClearer.registerCacheClearing(tableAndColumnNames::clear);
    }

    @VisibleForTesting
    Map<UpperCaseNameKey, String> getTableAndColumnNames() {
        return tableAndColumnNames;
    }


    @Override
    public Optional<String> getProductSchema() {
        return productSchemaProvider.getProductSchema();
    }


    @Override
    public Optional<String> getTableName(Connection connection, String logicalTableName) {
        checkArgument(!isEmpty(logicalTableName), "Table name is required");

        UpperCaseNameKey key = new UpperCaseNameKey(logicalTableName);
        String tableName = tableAndColumnNames.get(key);
        if (tableName == null) {
            cacheTableAndColumns(connection, logicalTableName);
            tableName = tableAndColumnNames.get(key);
        }
        return logMissing(tableName, logicalTableName, "table:" + logicalTableName);
    }

    @Override
    public Optional<String> getColumnName(Connection connection, String logicalTableName, String logicalColumnName) {
        checkArgument(!isEmpty(logicalTableName), "Table name is required");
        checkArgument(!isEmpty(logicalColumnName), "Column name is required");

        UpperCaseNameKey key = new UpperCaseNameKey(logicalTableName, logicalColumnName);
        String columnName = tableAndColumnNames.get(key);
        if (columnName == null) {
            cacheTableAndColumns(connection, logicalTableName);
            columnName = tableAndColumnNames.get(key);
        }
        return logMissing(columnName, logicalColumnName, "column:" + logicalTableName + "." + logicalColumnName);
    }

    private Optional<String> logMissing(final @Nullable String physical, final String logical, final String targetName) {
        Optional<String> dbObj = Optional.ofNullable(physical);
        if (!dbObj.isPresent()) {
            log.warn("Could not find the physical database object for the logically named '{}' aka '{}'. Is this expected database state?", logical, targetName);
        }
        return dbObj;
    }

    private void cacheTableAndColumns(Connection connection, String logicalTableName) {
        JdbcTableAndColumns tableAndColumns = tableInspector.inspectTableAndColumns(connection, getProductSchema(), logicalTableName);
        if (tableAndColumns.getTableName().isDefined()) {
            String realTableName = tableAndColumns.getTableName().get();
            UpperCaseNameKey tableKey = new UpperCaseNameKey(realTableName);
            tableAndColumnNames.put(tableKey, realTableName);

            for (String realColumnName : tableAndColumns.getColumnNames()) {
                UpperCaseNameKey columnKey = new UpperCaseNameKey(realTableName, realColumnName);
                tableAndColumnNames.put(columnKey, realColumnName);
            }
        }
    }


    /**
     * The key is an upper case version of logical table and (some times) logical column names
     */
    private static class UpperCaseNameKey {
        private final String tableName;
        private final String columnName;

        private UpperCaseNameKey(@Nonnull final String tableName) {
            this(tableName, null);
        }

        private UpperCaseNameKey(@Nonnull final String tableName, final @Nullable String columnName) {

            this.tableName = requireNonNull(tableName).toUpperCase();
            this.columnName = columnName == null ? null : columnName.toUpperCase();
        }

        @Override
        public boolean equals(final @Nullable Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || obj.getClass() != getClass()) {
                return false;
            }

            UpperCaseNameKey other = (UpperCaseNameKey) obj;

            return tableName.equals(other.tableName) && (columnName == null ? (other.columnName == null) : columnName.equals(other.columnName));
        }

        @Override
        public int hashCode() {
            int result = tableName.hashCode();
            result = 31 * result + (columnName != null ? columnName.hashCode() : 0);
            return result;
        }
    }

    private static boolean isEmpty(@Nullable String input) {
        return input == null || input.isEmpty();
    }
}
