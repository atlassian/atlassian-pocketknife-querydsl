package com.atlassian.pocketknife.internal.querydsl.util.fp;

import io.atlassian.fugue.Option;

import javax.annotation.Nullable;
import java.io.Serializable;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import static java.util.Objects.requireNonNull;

/**
 * Guava had some nice Functions and Predicates helpers.  Java 8 does not.  This plugs the gap
 */
public class Fp
{

    public static <S> Predicate<S> alwaysTrue()
    {
        return s -> true;
    }

    public static <T> Function<T, T> identity()
    {
        return t -> t;
    }


    /**
     * Returns the composition of two functions. For {@code f: A->B} and {@code g: B->C}, composition is defined as the
     * function h such that {@code h(a) == g(f(a))} for each {@code a}.
     *
     * @param g the second function to apply
     * @param f the first function to apply
     * @return the composition of {@code f} and {@code g}
     * @see <a href="//en.wikipedia.org/wiki/Function_composition">function composition</a>
     */
    public static <A, B, C> Function<A, C> compose(Function<B, C> g, Function<A, ? extends B> f)
    {
        return new FunctionComposition<>(g, f);
    }

    private static class FunctionComposition<A, B, C> implements Function<A, C>, Serializable
    {
        private final Function<B, C> g;
        private final Function<A, ? extends B> f;

        public FunctionComposition(Function<B, C> g, Function<A, ? extends B> f)
        {
            this.g = requireNonNull(g);
            this.f = requireNonNull(f);
        }

        @Override
        public C apply(A a)
        {
            return g.apply(f.apply(a));
        }

        @Override
        public boolean equals(@Nullable Object obj)
        {
            if (obj instanceof FunctionComposition)
            {
                FunctionComposition<?, ?, ?> that = (FunctionComposition<?, ?, ?>) obj;
                return f.equals(that.f) && g.equals(that.g);
            }
            return false;
        }

        @Override
        public int hashCode()
        {
            return f.hashCode() ^ g.hashCode();
        }

        @Override
        public String toString()
        {
            return g.toString() + "(" + f.toString() + ")";
        }

        private static final long serialVersionUID = 0;
    }


    /**
     * Returns the composition of a function and a predicate. For every {@code x}, the generated predicate returns
     * {@code predicate(function(x))}.
     *
     * @return the composition of the provided function and predicate
     */
    public static <A, B> Predicate<A> compose(Predicate<B> predicate, Function<A, ? extends B> function)
    {
        return new CompositionPredicate<>(predicate, function);
    }

    private static class CompositionPredicate<A, B> implements Predicate<A>, Serializable
    {
        final Predicate<B> p;
        final Function<A, ? extends B> f;

        private CompositionPredicate(Predicate<B> p, Function<A, ? extends B> f)
        {
            this.p = requireNonNull(p);
            this.f = requireNonNull(f);
        }

        @Override
        public boolean test(A a)
        {
            return p.test(f.apply(a));
        }

        @Override
        public boolean equals(@Nullable Object obj)
        {
            if (obj instanceof CompositionPredicate)
            {
                CompositionPredicate<?, ?> that = (CompositionPredicate<?, ?>) obj;
                return f.equals(that.f) && p.equals(that.p);
            }
            return false;
        }

        @Override
        public int hashCode()
        {
            return f.hashCode() ^ p.hashCode();
        }

        @Override
        public String toString()
        {
            return p.toString() + "(" + f.toString() + ")";
        }

        private static final long serialVersionUID = 0;
    }

    public static <T,R> Supplier<R> asSupplier(T value, Function<T, R> function) {
        R apply = function.apply(value);
        return () -> apply;
    }

    public static <T> Option<T> toOption(Optional<T> schema) {
        return Option.option(schema.orElse(null));
    }

    public static <T> Optional<T> toOptional(Option<T> schema) {
        return Optional.ofNullable(schema.getOrNull());
    }


}
