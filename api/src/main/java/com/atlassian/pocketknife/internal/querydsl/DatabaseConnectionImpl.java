package com.atlassian.pocketknife.internal.querydsl;

import com.atlassian.pocketknife.api.querydsl.DatabaseConnection;
import com.atlassian.pocketknife.api.querydsl.schema.DialectProvider;
import com.querydsl.core.Tuple;
import com.querydsl.core.types.Expression;
import com.querydsl.sql.RelationalPath;
import com.querydsl.sql.SQLQuery;
import com.querydsl.sql.SQLQueryFactory;
import com.querydsl.sql.dml.SQLDeleteClause;
import com.querydsl.sql.dml.SQLInsertClause;
import com.querydsl.sql.dml.SQLUpdateClause;

import javax.annotation.ParametersAreNonnullByDefault;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.util.concurrent.atomic.AtomicBoolean;

import static java.util.Objects.requireNonNull;

/**
 * An implementation of database connection
 */
@ParametersAreNonnullByDefault
public class DatabaseConnectionImpl implements DatabaseConnection {
    private final DialectProvider.Config dialectConfig;
    private final Connection connection;
    private final boolean managedConnection;
    private final AtomicBoolean closed;

    DatabaseConnectionImpl(final DialectProvider.Config dialectConfig, final Connection connection, boolean managedConnection) {
        this.dialectConfig = requireNonNull(dialectConfig);
        this.connection = requireNonNull(connection);
        this.managedConnection = managedConnection;
        this.closed = new AtomicBoolean(false);
    }

    @Override
    public <T> SQLQuery<T> select(final Expression<T> expression) {
        return queryFactory().select(expression);
    }

    @Override
    public SQLQuery<Tuple> select(final Expression<?>... expressions) {
        return queryFactory().select(expressions);
    }

    @Override
    public <T> SQLQuery<T> from(RelationalPath<T> path) {
        return queryFactory().selectFrom(path);
    }

    public SQLInsertClause insert(RelationalPath<?> entity) {
        return query().insert(entity);
    }

    public SQLDeleteClause delete(RelationalPath<?> entity) {
        return query().delete(entity);
    }

    public SQLUpdateClause update(RelationalPath<?> entity) {
        return query().update(entity);
    }

    @Override
    public SQLQueryFactory query() {
        return queryFactory();
    }

    private SQLQueryFactory queryFactory() {
        return new SQLQueryFactory(dialectConfig.getConfiguration(), () -> connection);
    }

    @Override
    public Connection getJdbcConnection() {
        return connection;
    }

    @Override
    public DialectProvider.Config getDialectConfig() {
        return dialectConfig;
    }

    @Override
    public boolean isInTransaction() {
        return managedConnection;
    }

    @Override
    public boolean isClosed() {
        return closed.get();
    }

    @Override
    public boolean isExternallyManaged() {
        return managedConnection;
    }

    @Override
    public boolean isAutoCommit() {
        try {
            return connection.getAutoCommit();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void setAutoCommit(final boolean autoCommit) {
        if (managedConnection) {
            throw new UnsupportedOperationException("This connection has a managed transaction");
        }
        try {
            connection.setAutoCommit(autoCommit);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void commit() {
        if (managedConnection) {
            throw new UnsupportedOperationException("This connection has a managed transaction");
        }
        try {
            connection.commit();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void rollback() {
        if (managedConnection) {
            throw new UnsupportedOperationException("This connection has a managed transaction");
        }
        try {
            connection.rollback();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() throws Exception {
        if (!closed.getAndSet(true)) {
            connection.close();
        }
    }


    @Override
    public void releaseSavepoint(Savepoint savepoint) {
        try {
            connection.releaseSavepoint(savepoint);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void rollback(Savepoint savepoint) {
        try {
            connection.rollback(savepoint);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Savepoint setSavepoint() {
        try {
            return connection.setSavepoint();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Savepoint setSavepoint(String name) {
        try {
            return connection.setSavepoint(name);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
