package com.atlassian.pocketknife.internal.querydsl.util;

/**
 * An alternative to {@link Void} that is actually once inhabited (whereas Void is inhabited by null, which causes
 * NPEs).
 *
 * This is in a future version of fugue but its handy so we copied it.  Relax DRY nazis, its 2 lines if you don't count
 * comments and braces
 */
public enum Unit
{
    /**
     * There is no Dana - only Unit
     */
    VALUE
}