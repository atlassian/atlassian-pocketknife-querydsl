package com.atlassian.pocketknife.internal.querydsl.dialect;

import io.atlassian.fugue.Pair;
import com.atlassian.pocketknife.api.querydsl.configuration.ConfigurationEnrichment;
import com.atlassian.pocketknife.api.querydsl.util.LoggingSqlListener;
import com.atlassian.pocketknife.internal.querydsl.schema.SchemaOverrideListener;
import com.atlassian.pocketknife.internal.querydsl.schema.SchemaOverrider;
import com.atlassian.pocketknife.internal.querydsl.schema.SchemaProvider;
import com.atlassian.pocketknife.internal.querydsl.util.MemoizingResettingReference;
import com.querydsl.sql.Configuration;
import com.querydsl.sql.H2Templates;
import com.querydsl.sql.HSQLDBTemplates;
import com.querydsl.sql.MySQLTemplates;
import com.querydsl.sql.OracleTemplates;
import com.querydsl.sql.PostgreSQLTemplates;
import com.querydsl.sql.SQLTemplates;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nullable;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * This is a dialect configuration that you can use to detect and build the QueryDSL dialect config.
 *
 * You can use this class to override the way the configuration is built via the {@link
 * #enrich(com.querydsl.sql.SQLTemplates.Builder)} and {@link #enrich(com.querydsl.sql.Configuration)} methods
 */
@Component
public class DefaultDialectConfiguration implements DialectConfiguration {
    private static final Logger log = LoggerFactory.getLogger(DefaultDialectConfiguration.class);
    private static Map<String, Pair<SQLTemplates.Builder, SupportedDatabase>> support = new LinkedHashMap<>();

    static {
        support.put(DialectHelper.DB_URL_IDENTIFIER_POSTGRES, Pair.pair(PostgreSQLTemplates.builder(), SupportedDatabase.POSTGRESSQL));
        support.put(DialectHelper.DB_URL_IDENTIFIER_ORACLE, Pair.pair(OracleTemplates.builder(), SupportedDatabase.ORACLE));
        support.put(DialectHelper.DB_URL_IDENTIFIER_HSQLDB, Pair.pair(HSQLDBTemplates.builder(), SupportedDatabase.HSQLDB));
        support.put(DialectHelper.DB_URL_IDENTIFIER_MYSQL, Pair.pair(MySQLTemplates.builder(), SupportedDatabase.MYSQL));
        support.put(DialectHelper.DB_URL_IDENTIFIER_H2, Pair.pair(H2Templates.builder(), SupportedDatabase.H2));
    }

    private final SchemaProvider schemaProvider;
    private final ConfigurationEnrichment configurationEnrichment;
    private final MemoizingResettingReference<Connection, Config> cachedConfigRef = new MemoizingResettingReference<>(this::detect);

    @Autowired
    public DefaultDialectConfiguration(final SchemaProvider schemaProvider, ConfigurationEnrichment configurationEnrichment) {
        this.schemaProvider = schemaProvider;
        this.configurationEnrichment = configurationEnrichment;
    }

    @Override
    public Config getDialectConfig(Connection connection) {
        // now get the dialect config for the database
        Config cachedConfig = cachedConfigRef.get(connection);
        SQLTemplates templates = cachedConfig.getSqlTemplates();
        //
        // construct a fresh configuration every time since its mutable per query
        // however the expensive bits have been cached earlier so that's ok
        //
        Configuration configuration = enrich(new Configuration(templates));
        return new Config(templates, configuration, cachedConfig.getDatabaseInfo());
    }

    private Config detect(final Connection connection) {
        Pair<SQLTemplates, SupportedDatabase> pair = buildTemplates(connection);
        SQLTemplates sqlTemplates = pair.left();
        Configuration configuration = new Configuration(sqlTemplates);
        return new Config(sqlTemplates, configuration, buildDatabaseInfo(pair.right(), connection));
    }

    @Override
    public SQLTemplates.Builder enrich(final SQLTemplates.Builder builder) {
        if (schemaProvider.getProductSchema().isPresent()) {
            // only print the schema IF we have a schema
            builder.printSchema();
        }

        return builder
                .newLineToSingleSpace()
                //
                // inActiveObjects has forced us into this!  grrrrr!
                //
                .quote()
                ;
    }

    @Override
    public Configuration enrich(final Configuration configuration) {
        // allow 3rd party enrichment as well here
        configurationEnrichment.getEnricher().enrich(configuration);
        //
        // things we need to work
        //
        // logging
        configuration.addListener(new LoggingSqlListener(configuration));
        //
        // allows us to capture table used and substitute real names with them
        configuration.addListener(new SchemaOverrideListener(configuration, new SchemaOverrider(schemaProvider)));
        return configuration;
    }

    private Pair<SQLTemplates, SupportedDatabase> buildTemplates(final Connection connection) {
        try {
            DatabaseMetaData metaData = connection.getMetaData();
            String connStr = metaData.getURL();
            Pair<SQLTemplates.Builder, SupportedDatabase> pair = getDBTemplate(connStr, metaData);
            if (pair == null) {
                throw new UnsupportedOperationException(String.format("Unable to detect QueryDSL template support for database %s", connStr));
            }
            SQLTemplates templates = enrich(pair.left()).build();
            return Pair.pair(templates, pair.right());

        } catch (SQLException e) {
            throw new RuntimeException("Unable to enquire on JDBC metadata to configure QueryDSL", e);
        }
    }

    /**
     * package private for testing
     */
    @Nullable
    Pair<SQLTemplates.Builder, SupportedDatabase> getDBTemplate(final String connStr, final DatabaseMetaData metaData) throws SQLException {
        Pair<SQLTemplates.Builder, SupportedDatabase> sqlTemplatePair;
        // instantiate SQLServerTemplate base on its version
        if (DialectHelper.isSQLServer(connStr)) {
            sqlTemplatePair = DialectHelper.getSQLServerDBTemplate(metaData);
        } else {
            sqlTemplatePair = getStaticSupportedDBTemplate(connStr);
        }

        if (sqlTemplatePair != null) {
            log.debug("SQL template has been initialized successfully {}", sqlTemplatePair.toString());
        } else {
            log.warn("System was unable to initialize SQL template for {}", connStr);
        }

        return sqlTemplatePair;
    }

    /**
     * Get supported DB template base on connection string except SQLServer
     * package private for testing
     */
    @Nullable
    Pair<SQLTemplates.Builder, SupportedDatabase> getStaticSupportedDBTemplate(final String connStr) {
        Pair<SQLTemplates.Builder, SupportedDatabase> pair = null;

        // which databases do we support
        for (String db : support.keySet()) {
            if (connStr.contains(db)) {
                pair = support.get(db);
                break;
            }
        }
        return pair;
    }

    private DatabaseInfo buildDatabaseInfo(final SupportedDatabase supportedDatabase, final Connection connection) {
        try {
            DatabaseMetaData metaData = connection.getMetaData();

            return new DatabaseInfo(supportedDatabase,
                    metaData.getDatabaseProductName(),
                    metaData.getDatabaseProductVersion(),
                    metaData.getDatabaseMajorVersion(),
                    metaData.getDatabaseMinorVersion(),
                    metaData.getDriverName(),
                    metaData.getDriverMajorVersion(),
                    metaData.getDriverMinorVersion());
        } catch (SQLException e) {
            throw new RuntimeException("Unable to enquire on JDBC metadata to determine DatabaseInfo", e);
        }
    }
}
