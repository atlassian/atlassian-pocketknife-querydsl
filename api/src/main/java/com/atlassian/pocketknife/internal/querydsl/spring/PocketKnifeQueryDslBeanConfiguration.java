package com.atlassian.pocketknife.internal.querydsl.spring;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.pocketknife.api.querydsl.DatabaseAccessor;
import com.atlassian.pocketknife.api.querydsl.DatabaseCompatibilityKit;
import com.atlassian.pocketknife.api.querydsl.DatabaseConnectionConverter;
import com.atlassian.pocketknife.api.querydsl.EitherAwareDatabaseAccessor;
import com.atlassian.pocketknife.api.querydsl.OptionalAwareDatabaseAccessor;
import com.atlassian.pocketknife.api.querydsl.configuration.ConfigurationEnrichment;
import com.atlassian.pocketknife.api.querydsl.schema.DialectProvider;
import com.atlassian.pocketknife.api.querydsl.schema.SchemaStateProvider;
import com.atlassian.pocketknife.api.querydsl.stream.StreamingQueryFactory;
import com.atlassian.pocketknife.internal.querydsl.DatabaseAccessorImpl;
import com.atlassian.pocketknife.internal.querydsl.DatabaseCompatibilityKitImpl;
import com.atlassian.pocketknife.internal.querydsl.DatabaseConnectionConverterImpl;
import com.atlassian.pocketknife.internal.querydsl.EitherAwareDatabaseAccessorImpl;
import com.atlassian.pocketknife.internal.querydsl.OptionalAwareDatabaseAccessorImpl;
import com.atlassian.pocketknife.internal.querydsl.cache.PKQCacheClearer;
import com.atlassian.pocketknife.internal.querydsl.cache.PKQCacheClearerImpl;
import com.atlassian.pocketknife.internal.querydsl.configuration.ConfigurationEnrichmentImpl;
import com.atlassian.pocketknife.internal.querydsl.dialect.DefaultDialectConfiguration;
import com.atlassian.pocketknife.internal.querydsl.dialect.DialectConfiguration;
import com.atlassian.pocketknife.internal.querydsl.schema.DatabaseSchemaCreation;
import com.atlassian.pocketknife.internal.querydsl.schema.DatabaseSchemaCreationImpl;
import com.atlassian.pocketknife.internal.querydsl.schema.DefaultSchemaProvider;
import com.atlassian.pocketknife.internal.querydsl.schema.JdbcTableInspector;
import com.atlassian.pocketknife.internal.querydsl.schema.ProductSchemaProvider;
import com.atlassian.pocketknife.internal.querydsl.schema.SchemaProvider;
import com.atlassian.pocketknife.internal.querydsl.schema.SchemaStateProviderImpl;
import com.atlassian.pocketknife.internal.querydsl.stream.StreamingQueryFactoryImpl;
import com.atlassian.sal.api.rdbms.TransactionalExecutorFactory;
import org.osgi.framework.BundleContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * For consumers using Spring OSGi Java config rather than Spring Scanner.
 * <p>
 * See the fields below for what you'll need to import and export from/to OSGi yourself.
 * <p>
 * To consume this configuration file from your own, put this annotation above one of your config files
 * {@code @Import(com.atlassian.pocketknife.internal.querydsl.spring.PocketKnifeQueryDslBeanConfiguration.class)}
 */
@Configuration
public class PocketKnifeQueryDslBeanConfiguration {

    // You'll need to import these from OSGi yourself
    EventPublisher eventPublisher;
    TransactionalExecutorFactory transactionalExecutorFactory;
    // You'll need to export these from OSGi yourself
    SchemaStateProvider schemaStateProvider;

    @Bean
    public ConfigurationEnrichment configurationEnrichment() {
        return new ConfigurationEnrichmentImpl();
    }

    @Bean
    public DatabaseAccessor databaseAccessor(
            DatabaseConnectionConverter databaseConnectionConverter,
            DatabaseSchemaCreation databaseSchemaCreation,
            TransactionalExecutorFactory transactionalExecutorFactory
    ) {
        return new DatabaseAccessorImpl(databaseConnectionConverter, transactionalExecutorFactory, databaseSchemaCreation);
    }

    @Bean
    public DatabaseCompatibilityKit databaseCompatibilityKit(DialectConfiguration dialectConfiguration) {
        return new DatabaseCompatibilityKitImpl(dialectConfiguration);
    }

    @Bean
    public DatabaseConnectionConverter databaseConnectionConverter(DialectProvider dialectProvider) {
        return new DatabaseConnectionConverterImpl(dialectProvider);
    }

    @Bean
    public DatabaseSchemaCreation databaseSchemaCreation(
            BundleContext bundleContext,
            PKQCacheClearer pkqCacheClearer
    ) {
        return new DatabaseSchemaCreationImpl(bundleContext, pkqCacheClearer);
    }

    @Bean
    public DialectConfiguration dialectConfiguration(
            ConfigurationEnrichment configurationEnrichment,
            SchemaProvider schemaProvider
    ) {
        return new DefaultDialectConfiguration(schemaProvider, configurationEnrichment);
    }

    @Bean
    public EitherAwareDatabaseAccessor eitherAwareDatabaseAccessor(DatabaseAccessor databaseAccessor) {
        return new EitherAwareDatabaseAccessorImpl(databaseAccessor);
    }

    @Bean
    public JdbcTableInspector jdbcTableInspector() {
        return new JdbcTableInspector();
    }

    @Bean
    public OptionalAwareDatabaseAccessor optionalAwareDatabaseAccessor(DatabaseAccessor databaseAccessor) {
        return new OptionalAwareDatabaseAccessorImpl(databaseAccessor);
    }

    @Bean
    public SchemaProvider schemaProvider(
            JdbcTableInspector jdbcTableInspector,
            ProductSchemaProvider productSchemaProvider,
            PKQCacheClearer pkqCacheClearer
    ) {
        return new DefaultSchemaProvider(productSchemaProvider, jdbcTableInspector, pkqCacheClearer);
    }

    @Bean
    public SchemaStateProvider schemaStateProvider(
            ProductSchemaProvider productSchemaProvider,
            JdbcTableInspector jdbcTableInspector
    ) {
        return new SchemaStateProviderImpl(productSchemaProvider, jdbcTableInspector);
    }

    @Bean
    public StreamingQueryFactory streamingQueryFactory() {
        return new StreamingQueryFactoryImpl();
    }

    @Bean
    public PKQCacheClearer pkqCacheClearer(EventPublisher eventPublisher) {
        return new PKQCacheClearerImpl(eventPublisher);
    }

    @Bean
    public ProductSchemaProvider productSchemaProvider(TransactionalExecutorFactory transactionalExecutorFactory) {
        return new ProductSchemaProvider(transactionalExecutorFactory);
    }
}
