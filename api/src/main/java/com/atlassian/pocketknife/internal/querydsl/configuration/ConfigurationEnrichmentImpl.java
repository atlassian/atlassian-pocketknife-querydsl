package com.atlassian.pocketknife.internal.querydsl.configuration;


import com.atlassian.pocketknife.api.querydsl.configuration.ConfigurationEnrichment;
import com.atlassian.pocketknife.spi.querydsl.configuration.ConfigurationEnricher;
import com.querydsl.sql.Configuration;
import org.springframework.stereotype.Component;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.concurrent.atomic.AtomicReference;

import static java.util.Objects.requireNonNull;

@Component
@ParametersAreNonnullByDefault
public class ConfigurationEnrichmentImpl implements ConfigurationEnrichment {

    private final AtomicReference<ConfigurationEnricher> enricher = new AtomicReference<>(new NoopEnricher());

    @Override
    public ConfigurationEnricher getEnricher() {
        return enricher.get();
    }

    @Override
    public void setEnricher(ConfigurationEnricher enricher) {
        requireNonNull(enricher);
        this.enricher.set(enricher);
    }

    @ParametersAreNonnullByDefault
    private class NoopEnricher implements ConfigurationEnricher {
        @Override
        public Configuration enrich(Configuration configuration) {
            return configuration;
        }
    }
}
