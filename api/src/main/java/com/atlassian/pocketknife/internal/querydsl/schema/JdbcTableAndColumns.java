package com.atlassian.pocketknife.internal.querydsl.schema;

import io.atlassian.fugue.Option;

import java.util.LinkedHashSet;

/**
 * A simple holder class of table and column information as obtained from JDBC
 */
public class JdbcTableAndColumns {
    private final Option<String> tableName;
    private final LinkedHashSet<String> tableColumns;

    public JdbcTableAndColumns(Option<String> tableName, LinkedHashSet<String> tableColumns) {
        this.tableName = tableName;
        this.tableColumns = tableColumns;
    }

    public LinkedHashSet<String> getColumnNames() {
        return tableColumns;
    }

    public Option<String> getTableName() {
        return tableName;
    }
}
