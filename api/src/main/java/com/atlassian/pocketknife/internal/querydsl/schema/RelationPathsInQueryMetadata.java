package com.atlassian.pocketknife.internal.querydsl.schema;

import io.atlassian.fugue.Option;
import com.atlassian.pocketknife.internal.querydsl.util.Unit;
import com.querydsl.core.JoinExpression;
import com.querydsl.core.QueryMetadata;
import com.querydsl.core.types.Constant;
import com.querydsl.core.types.Expression;
import com.querydsl.core.types.FactoryExpression;
import com.querydsl.core.types.Operation;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.ParamExpression;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.SubQueryExpression;
import com.querydsl.core.types.TemplateExpression;
import com.querydsl.core.types.Visitor;
import com.querydsl.sql.RelationalPath;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static io.atlassian.fugue.Option.option;

/**
 * This can pull apart a query via its metadata and extract the relational paths
 * used within it.
 */
public class RelationPathsInQueryMetadata {

    private static final Logger log = LoggerFactory.getLogger(RelationPathsInQueryMetadata.class);

    private static final Runnable NOOP = () -> {
    };

    /**
     * Captures the relational paths that are in a query
     *
     * @param metadata the query metadata
     *
     * @return the set of relational paths in the query
     */
    public Set<RelationalPath<?>> capture(QueryMetadata metadata) {

        return new CaptureVisitor().traverse(metadata, new CaptureCtx()).getRelationalPaths();
    }

    /**
     * This is passed into each visitor method as context
     */
    static class CaptureCtx {
        private final Set<RelationalPath<?>> relationalPaths = new HashSet<>();

        public Set<RelationalPath<?>> getRelationalPaths() {
            return relationalPaths;
        }
    }

    /**
     * Our visitor knows who to pull apart a set fo expressions and capture the
     * relational paths used
     */
    static class CaptureVisitor implements Visitor<Unit, CaptureCtx> {

        private CaptureCtx traverse(QueryMetadata metadata, CaptureCtx context) {
            //
            // the following are the elements of the query that may contain RelationalPath objects
            // or have expressions that back point to RelationalPath objects
            //
            final Option<Expression<?>> select = option(metadata.getProjection());
            final Option<List<JoinExpression>> joins = option(metadata.getJoins());
            final Option<Predicate> where = option(metadata.getWhere());
            final Option<List<? extends Expression<?>>> groupBys = option(metadata.getGroupBy());
            final Option<Predicate> having = option(metadata.getHaving());
            final Option<List<OrderSpecifier<?>>> orderBys = option(metadata.getOrderBy());

            select.forEach(e -> handle(e, context));

            joins.forEach(list -> list.forEach(join -> handle(join.getTarget(), context)));

            where.forEach(e -> handle(e, context));

            groupBys.forEach(list -> list.forEach(e -> handle(e, context)));

            having.forEach(e -> handle(e, context));

            orderBys.forEach(list -> list.forEach(ob -> handle(ob.getTarget(), context)));

            return context;
        }

        private void handle(Expression<?> expr, CaptureCtx context) {
            if (expr != null) {
                expr.accept(this, context);
            }
        }

        private void acceptExpressions(List<Expression<?>> args, CaptureCtx context) {
            // this explodes the expressions back into specific types and visits each of them again
            for (Expression<?> arg : args) {
                arg.accept(this, context);
            }
        }

        private Unit enter(String place, Expression<?> expr, Runnable runnable) {
            if (log.isDebugEnabled()) {
                log.debug("Entering {} - type {}", place, expr.getClass().getName());
            }
            runnable.run();
            if (log.isDebugEnabled()) {
                log.debug("\tExiting {} - type {}", place, expr.getClass().getName());
            }
            return Unit.VALUE;
        }

        @Override
        public Unit visit(Path<?> expr, CaptureCtx context) {
            return enter("Path", expr, () -> {
                if (expr instanceof RelationalPath) {
                    RelationalPath relationalPath = (RelationalPath) expr;
                    context.getRelationalPaths().add(relationalPath);
                }
                Option<Path<?>> parent = option(expr.getMetadata()).flatMap(md -> option(md.getParent()));
                parent.forEach(parentPath -> {
                    // the root of top level is not null but itself sometimes
                    if (parentPath != expr) {
                        visit(parentPath, context);
                    }
                });
            });
        }

        @Override
        public Unit visit(FactoryExpression<?> expr, CaptureCtx context) {
            return enter("FactoryExpression", expr, () -> {
                List<Expression<?>> args = expr.getArgs();
                acceptExpressions(args, context);
            });
        }

        @Override
        public Unit visit(Operation<?> expr, CaptureCtx context) {
            return enter("Operation", expr, () -> {
                List<Expression<?>> args = expr.getArgs();
                acceptExpressions(args, context);
            });
        }

        @Override
        public Unit visit(SubQueryExpression<?> expr, CaptureCtx context) {
            return enter("SubQueryExpression", expr, () -> {
                QueryMetadata metadata = expr.getMetadata();
                traverse(metadata, context);
            });
        }

        @Override
        public Unit visit(Constant<?> expr, CaptureCtx context) {
            return enter("Constant", expr, NOOP);
        }

        @Override
        public Unit visit(ParamExpression<?> expr, CaptureCtx context) {
            return enter("ParamExpression", expr, NOOP);
        }

        @Override
        public Unit visit(TemplateExpression<?> expr, CaptureCtx context) {
            return enter("TemplateExpression", expr, NOOP);
        }
    }
}
