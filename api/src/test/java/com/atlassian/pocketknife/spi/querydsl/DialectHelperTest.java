package com.atlassian.pocketknife.spi.querydsl;

import io.atlassian.fugue.Pair;
import com.atlassian.pocketknife.api.querydsl.schema.DialectProvider;
import com.atlassian.pocketknife.internal.querydsl.dialect.DialectHelper;
import com.querydsl.sql.SQLServer2005Templates;
import com.querydsl.sql.SQLServer2008Templates;
import com.querydsl.sql.SQLServer2012Templates;
import com.querydsl.sql.SQLServerTemplates;
import com.querydsl.sql.SQLTemplates;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import java.sql.DatabaseMetaData;
import java.sql.SQLException;

import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class DialectHelperTest
{

    private DatabaseMetaData databaseMetaData;

    @Before
    public void init()
    {
        databaseMetaData = mock(DatabaseMetaData.class);
    }

    @Test
    public void test_connection_string_is_not_sqlserver()
    {
        String mysqlConnStr = "jdbc:mysql://dbserver:3306/schema";
        boolean isSQLServer = DialectHelper.isSQLServer(mysqlConnStr);
        assertThat(isSQLServer, Matchers.is(false));
    }

    @Test
    public void test_connection_string_is_sqlserver()
    {
        String mssqlConnStr = "jdbc:jtds:sqlserver://dbserver:1433/schema";
        boolean isSQLServer = DialectHelper.isSQLServer(mssqlConnStr);
        assertThat(isSQLServer, Matchers.is(true));
    }

    @SuppressWarnings("ConstantConditions")
    @Test(expected = NullPointerException.class)
    public void test_connection_string_is_sqlserver_connstr_is_null()
    {
        String connStr = null;
        DialectHelper.isSQLServer(connStr);
    }

    @Test
    public void test_get_sql_server_template_by_version_2005() throws SQLException
    {
        Class<SQLServer2005Templates> expectedTemplateType = SQLServer2005Templates.class;
        when(databaseMetaData.getDatabaseMajorVersion()).thenReturn(DialectHelper.SQLSERVER_2005);

        assert_server_template_type(expectedTemplateType);
    }

    @Test
    public void test_get_sql_server_template_by_version_2008() throws SQLException
    {
        Class<SQLServer2008Templates> expectedTemplateType = SQLServer2008Templates.class;
        when(databaseMetaData.getDatabaseMajorVersion()).thenReturn(DialectHelper.SQLSERVER_2008);

        assert_server_template_type(expectedTemplateType);
    }

    @Test
    public void test_get_sql_server_template_by_version_2012() throws SQLException
    {
        Class<SQLServer2012Templates> expectedTemplateType = SQLServer2012Templates.class;
        when(databaseMetaData.getDatabaseMajorVersion()).thenReturn(DialectHelper.SQLSERVER_2012);

        assert_server_template_type(expectedTemplateType);
    }

    @Test
    public void test_get_sql_server_template_by_version_newer_than_2012() throws SQLException
    {
        Class<SQLServer2012Templates> expectedTemplateType = SQLServer2012Templates.class;
        when(databaseMetaData.getDatabaseMajorVersion()).thenReturn(DialectHelper.SQLSERVER_2012 + 1);

        assert_server_template_type(expectedTemplateType);
    }

    @Test
    public void test_get_sql_server_template_by_version_older_than_2005() throws SQLException
    {
        Class<SQLServerTemplates> expectedTemplateType = SQLServerTemplates.class;
        when(databaseMetaData.getDatabaseMajorVersion()).thenReturn(DialectHelper.SQLSERVER_2005 - 1);

        assert_server_template_type(expectedTemplateType);
    }

    private <T extends SQLTemplates> void assert_server_template_type(Class<T> expectedTemplateType) throws SQLException
    {
        Pair<SQLTemplates.Builder, DialectProvider.SupportedDatabase> serverTemplate = DialectHelper.getSQLServerDBTemplate(databaseMetaData);
        assertThat(serverTemplate.left().build(), Matchers.instanceOf(expectedTemplateType));
    }
}
