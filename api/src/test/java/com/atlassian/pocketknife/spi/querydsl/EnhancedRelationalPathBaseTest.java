package com.atlassian.pocketknife.spi.querydsl;

import com.atlassian.pocketknife.internal.querydsl.tables.domain.QEmailSettings;
import com.google.common.collect.Lists;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.sql.ColumnMetadata;
import com.querydsl.sql.PrimaryKey;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import java.sql.Types;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

public class EnhancedRelationalPathBaseTest {

    QEmailSettings EMAIL_SETTINGS;

    @Before
    public void setUp() throws Exception {
        EMAIL_SETTINGS = new QEmailSettings();
    }

    @SuppressWarnings({"ConstantConditions", "unchecked"})
    @Test
    public void test_path_construction() throws Exception {
        // assert
        assertThat(EMAIL_SETTINGS.getTableName(), equalTo("QEMAILSETTINGS_TABLE"));
        assertThat(EMAIL_SETTINGS.getSchemaAndTable().getTable(), equalTo("QEMAILSETTINGS_TABLE"));

        assertThat(EMAIL_SETTINGS.getSchemaName(), equalTo(""));
        assertThat(EMAIL_SETTINGS.getSchemaAndTable().getSchema(), equalTo(""));

        PrimaryKey<QEmailSettings> primaryKey = EMAIL_SETTINGS.getPrimaryKey();
        assertThat(primaryKey, notNullValue());

        assertPathsPresent(primaryKey.getLocalColumns(), EMAIL_SETTINGS.ID);


        assertPathsPresent(EMAIL_SETTINGS.getColumns(),
                EMAIL_SETTINGS.ID,
                EMAIL_SETTINGS.CREATED,
                EMAIL_SETTINGS.DESCRIPTION,
                EMAIL_SETTINGS.EMAIL_ADDRESS,
                EMAIL_SETTINGS.ENABLED,
                EMAIL_SETTINGS.JIRA_MAIL_SERVER_ID,
                EMAIL_SETTINGS.LAST_PROCEEDED_TIME,
                EMAIL_SETTINGS.ON_DEMAND,
                EMAIL_SETTINGS.REQUEST_TYPE_ID,
                EMAIL_SETTINGS.SERVICE_DESK_ID,
                EMAIL_SETTINGS.UPDATED_DATE,
                EMAIL_SETTINGS.UPDATED_TIME,
                EMAIL_SETTINGS.CURRENT_STATUS,
                EMAIL_SETTINGS.DEFAULT_STATUS
        );

        assertPathsPresent(EMAIL_SETTINGS.getAllNonPrimaryKeyColumns(),
                EMAIL_SETTINGS.CREATED,
                EMAIL_SETTINGS.DESCRIPTION,
                EMAIL_SETTINGS.EMAIL_ADDRESS,
                EMAIL_SETTINGS.ENABLED,
                EMAIL_SETTINGS.JIRA_MAIL_SERVER_ID,
                EMAIL_SETTINGS.LAST_PROCEEDED_TIME,
                EMAIL_SETTINGS.ON_DEMAND,
                EMAIL_SETTINGS.REQUEST_TYPE_ID,
                EMAIL_SETTINGS.SERVICE_DESK_ID,
                EMAIL_SETTINGS.UPDATED_DATE,
                EMAIL_SETTINGS.UPDATED_TIME,
                EMAIL_SETTINGS.CURRENT_STATUS,
                EMAIL_SETTINGS.DEFAULT_STATUS
        );

    }

    @Test
    public void test_metadata_recording() throws Exception {
        // act
        assertColumnMetadata(EMAIL_SETTINGS, EMAIL_SETTINGS.ID, "ID", false, Types.INTEGER, true);

        assertColumnMetadata(EMAIL_SETTINGS, EMAIL_SETTINGS.CREATED, "CREATED", false, Types.TIMESTAMP);
        assertColumnMetadata(EMAIL_SETTINGS, EMAIL_SETTINGS.DESCRIPTION, "DESCRIPTION", false, Types.VARCHAR);
        assertColumnMetadata(EMAIL_SETTINGS, EMAIL_SETTINGS.EMAIL_ADDRESS, "EMAIL_ADDRESS", true, Types.VARCHAR);
        assertColumnMetadata(EMAIL_SETTINGS, EMAIL_SETTINGS.ENABLED, "ENABLED", true, Types.BOOLEAN);
        assertColumnMetadata(EMAIL_SETTINGS, EMAIL_SETTINGS.JIRA_MAIL_SERVER_ID, "JIRA_MAIL_SERVER_ID", true, Types.BIGINT);
        assertColumnMetadata(EMAIL_SETTINGS, EMAIL_SETTINGS.LAST_PROCEEDED_TIME, "LAST_PROCEEDED_TIME", true, Types.BIGINT);
        assertColumnMetadata(EMAIL_SETTINGS, EMAIL_SETTINGS.ON_DEMAND, "ON_DEMAND", false, Types.BOOLEAN);
        assertColumnMetadata(EMAIL_SETTINGS, EMAIL_SETTINGS.REQUEST_TYPE_ID, "REQUEST_TYPE_ID", true, Types.INTEGER);
        assertColumnMetadata(EMAIL_SETTINGS, EMAIL_SETTINGS.SERVICE_DESK_ID, "SERVICE_DESK_ID", true, Types.INTEGER);
        assertColumnMetadata(EMAIL_SETTINGS, EMAIL_SETTINGS.UPDATED_DATE, "UPDATED_DATE", false, Types.DATE);
        assertColumnMetadata(EMAIL_SETTINGS, EMAIL_SETTINGS.UPDATED_TIME, "UPDATED_TIME", false, Types.TIME);

        // this is a specific declaration of type
        assertColumnMetadata(EMAIL_SETTINGS, EMAIL_SETTINGS.CURRENT_STATUS, "CURRENT_STATUS", true, Types.JAVA_OBJECT);
        // this is a sensible default
        assertColumnMetadata(EMAIL_SETTINGS, EMAIL_SETTINGS.DEFAULT_STATUS, "DEFAULT_STATUS", true, Types.VARCHAR);

    }

    @Test
    public void test_number_type_support() throws Exception {
        // in the wild we have seen people use Long.TYPE as well as Long.class - so we handle it
        class QNumber extends EnhancedRelationalPathBase<QNumber> {
            public QNumber() {
                super(QNumber.class, "QNumber");
            }

            public final NumberPath<Integer> INTEGER_CLASS = createNumber("INTEGER_CLASS", Integer.class);
            public final NumberPath<Integer> INTEGER_TYPE = createNumber("INTEGER_TYPE", Integer.TYPE);

            public final NumberPath<Long> LONG_CLASS = createNumber("LONG_CLASS", Long.class);
            public final NumberPath<Long> LONG_TYPE = createNumber("LONG_TYPE", Long.TYPE);

            public final NumberPath<Double> DOUBLE_CLASS = createNumber("DOUBLE_CLASS", Double.class);
            public final NumberPath<Double> DOUBLE_TYPE = createNumber("DOUBLE_TYPE", Double.TYPE);

            public final NumberPath<Float> FLOAT_CLASS = createNumber("FLOAT_CLASS", Float.class);
            public final NumberPath<Float> FLOAT_TYPE = createNumber("FLOAT_TYPE", Float.TYPE);

        }

        QNumber TABLE = new QNumber();

        assertColumnMetadata(TABLE, TABLE.INTEGER_CLASS, "INTEGER_CLASS", true, Types.INTEGER);
        assertColumnMetadata(TABLE, TABLE.INTEGER_TYPE, "INTEGER_TYPE", true, Types.INTEGER);

        assertColumnMetadata(TABLE, TABLE.LONG_CLASS, "LONG_CLASS", true, Types.BIGINT);
        assertColumnMetadata(TABLE, TABLE.LONG_TYPE, "LONG_TYPE", true, Types.BIGINT);

        assertColumnMetadata(TABLE, TABLE.DOUBLE_CLASS, "DOUBLE_CLASS", true, Types.DOUBLE);
        assertColumnMetadata(TABLE, TABLE.DOUBLE_TYPE, "DOUBLE_TYPE", true, Types.DOUBLE);

        assertColumnMetadata(TABLE, TABLE.FLOAT_CLASS, "FLOAT_CLASS", true, Types.DECIMAL);
        assertColumnMetadata(TABLE, TABLE.FLOAT_TYPE, "FLOAT_TYPE", true, Types.DECIMAL);


    }

    private void assertColumnMetadata(EnhancedRelationalPathBase<?> classUnderTest, final Path<?> path, final String name, final boolean nullable, int jdbcType) {
        assertColumnMetadata(classUnderTest, path, name, nullable, jdbcType, false);
    }

    @SuppressWarnings("ConstantConditions")
    private void assertColumnMetadata(EnhancedRelationalPathBase<?> classUnderTest, final Path<?> path, final String name, final boolean nullable, int jdbcType, boolean asPK) {
        ColumnMetadata metadata = classUnderTest.getMetadata(path);
        assertThat("column name", metadata.getName(), equalTo(name));
        assertThat("jdbc type", metadata.getJdbcType(), equalTo(jdbcType));
        assertThat("nullable", metadata.isNullable(), equalTo(nullable));
        assertIsPK(classUnderTest, path, asPK);

    }

    @SuppressWarnings("ConstantConditions")
    private void assertIsPK(EnhancedRelationalPathBase<?> classUnderTest, final Path<?> path, final boolean asPK) {
        PrimaryKey<?> primaryKey = classUnderTest.getPrimaryKey();
        if (asPK) {
            assertThat(primaryKey.getLocalColumns().contains(path), equalTo(true));
        }
    }

    private void assertPathsPresent(final Path<?>[] columns, final Path<?>... paths) {
        assertThat(columns, notNullValue());
        assertPathsPresent(Lists.newArrayList(paths), paths);
    }

    private void assertPathsPresent(List<? extends Path<?>> columns, final Path<?>... paths) {
        assertThat(columns, notNullValue());
        assertThat("path columns are not the right size", columns.size(), equalTo(paths.length));
        for (Path<?> path : paths) {
            assertThat(columns.contains(path), Matchers.is(true));
        }
    }

}