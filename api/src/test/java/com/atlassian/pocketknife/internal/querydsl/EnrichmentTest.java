package com.atlassian.pocketknife.internal.querydsl;

import com.atlassian.pocketknife.api.querydsl.DatabaseAccessor;
import com.atlassian.pocketknife.internal.querydsl.mocks.TestingContext;
import com.atlassian.pocketknife.internal.querydsl.tables.domain.QTypeTest;
import com.atlassian.pocketknife.spi.querydsl.configuration.ConfigurationEnricher;
import com.querydsl.core.Tuple;
import com.querydsl.sql.types.EnumByNameType;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.pocketknife.api.querydsl.util.OnRollback.NOOP;
import static com.atlassian.pocketknife.internal.querydsl.tables.domain.QTypeTest.typeTest;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * Tests the basic querying that happens
 */
public class EnrichmentTest {
    private DatabaseAccessor databaseAccessor;
    private TestingContext testingContext;

    @Before
    public void setUp() throws Exception {
        testingContext = TestingContext.forH2();
        databaseAccessor = testingContext.getDatabaseAccessor();
    }


    @Test
    public void testBasicEnumEnrichment() throws Exception {
        ConfigurationEnricher enricher = configuration -> {
            configuration.register(new EnumByNameType<>(QTypeTest.SimpleAnimals.class));
            configuration.register(new EnumByNameType<>(QTypeTest.ComplexAnimals.class));
            return configuration;
        };

        testingContext.getConfigurationEnrichment().setEnricher(enricher);

        databaseAccessor.runInNewTransaction(dbConn -> dbConn
                .insert(typeTest)
                .set(typeTest.t1, QTypeTest.SimpleAnimals.RATS)
                .set(typeTest.t2, QTypeTest.ComplexAnimals.HUMPY_BACK_CAMELS)
                .execute(), NOOP);

        Tuple data = databaseAccessor.runInNewTransaction(dbConn -> dbConn.select(typeTest.t1, typeTest.t2).from(typeTest).fetchOne(), NOOP);

        assertThat(data.get(typeTest.t1), equalTo(QTypeTest.SimpleAnimals.RATS));
        assertThat(data.get(typeTest.t2), equalTo(QTypeTest.ComplexAnimals.HUMPY_BACK_CAMELS));
    }

}

