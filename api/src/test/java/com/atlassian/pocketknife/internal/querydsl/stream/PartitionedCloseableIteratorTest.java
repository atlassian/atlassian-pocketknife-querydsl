package com.atlassian.pocketknife.internal.querydsl.stream;

import com.atlassian.pocketknife.internal.querydsl.stream.PartitionedCloseableIterable.PartitionedCloseableIterator;
import com.google.common.collect.ImmutableList;
import com.mysema.commons.lang.CloseableIterator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;
import java.util.NoSuchElementException;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.Collections.emptyList;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class PartitionedCloseableIteratorTest {

    private static final List<Integer> RESULTS_1 = ImmutableList.of(1, 2);
    private static final List<Integer> RESULTS_2 = ImmutableList.of(3, 4);

    @Mock
    private CloseableIterator<Integer> closeableIterator1;

    @Mock
    private CloseableIterator<Integer> closeableIterator2;

    @Before
    public void setup() {
        final List<Integer> results1 = newArrayList(RESULTS_1);
        final List<Integer> results2 = newArrayList(RESULTS_2);

        doAnswer(invocation -> !results1.isEmpty()).when(closeableIterator1).hasNext();
        doAnswer(invocation -> !results2.isEmpty()).when(closeableIterator2).hasNext();

        doAnswer(invocation -> results1.remove(0)).when(closeableIterator1).next();
        doAnswer(invocation -> results2.remove(0)).when(closeableIterator2).next();
    }

    @Test
    public void null_is_treated_as_empty_list() {
        final PartitionedCloseableIterator<Integer> closeableIterator = new PartitionedCloseableIterator<>(null);

        assertThat(closeableIterator.hasNext(), is(false));
    }

    @Test
    public void hasNext__false_when_empty_list() {
        final PartitionedCloseableIterator<Integer> closeableIterator = new PartitionedCloseableIterator<>(emptyList());

        assertThat(closeableIterator.hasNext(), is(false));
    }

    @Test(expected = NoSuchElementException.class)
    public void next__exception_when_empty_list() {
        new PartitionedCloseableIterator<>(emptyList()).next();
    }

    @Test
    public void hasNext_and_next__iterates_correctly_through_single_list() {
        final PartitionedCloseableIterator<Integer> closeableIterator = new PartitionedCloseableIterator<>(ImmutableList.of(closeableIterator1));

        assertThat(closeableIterator.hasNext(), is(true));
        assertThat(closeableIterator.next(), is(1));
        assertThat(closeableIterator.hasNext(), is(true));
        assertThat(closeableIterator.next(), is(2));
        assertThat(closeableIterator.hasNext(), is(false));
    }

    @Test
    public void hasNext_and_next__iterates_correctly_through_multiple_lists() {
        final PartitionedCloseableIterator<Integer> closeableIterator = new PartitionedCloseableIterator<>(ImmutableList.of(closeableIterator1, closeableIterator2));

        assertThat(closeableIterator.hasNext(), is(true));
        assertThat(closeableIterator.next(), is(1));
        assertThat(closeableIterator.hasNext(), is(true));
        assertThat(closeableIterator.next(), is(2));
        assertThat(closeableIterator.hasNext(), is(true));
        assertThat(closeableIterator.next(), is(3));
        assertThat(closeableIterator.hasNext(), is(true));
        assertThat(closeableIterator.next(), is(4));
        assertThat(closeableIterator.hasNext(), is(false));
    }

    @Test
    public void close__iterates_correctly_through_single_list() {
        final PartitionedCloseableIterator<Integer> closeableIterator = new PartitionedCloseableIterator<>(ImmutableList.of(closeableIterator1));

        closeableIterator.close();

        verify(closeableIterator1).close();
    }

    @Test
    public void close__iterates_correctly_through_multiple_lists() {
        final PartitionedCloseableIterator<Integer> closeableIterator = new PartitionedCloseableIterator<>(ImmutableList.of(closeableIterator1, closeableIterator2));

        closeableIterator.close();

        verify(closeableIterator1).close();
        verify(closeableIterator2).close();
    }
}
