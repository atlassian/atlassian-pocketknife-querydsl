package com.atlassian.pocketknife.internal.querydsl.mocks;

import com.atlassian.pocketknife.internal.querydsl.schema.SchemaProvider;

import javax.annotation.ParametersAreNonnullByDefault;
import java.sql.Connection;
import java.util.Optional;

@ParametersAreNonnullByDefault
public class MockSchemaProvider implements SchemaProvider {

    private final Optional<String> schema;

    public MockSchemaProvider() {
        this(Optional.of("PUBLIC"));
    }

    public MockSchemaProvider(Optional<String> schema) {
        this.schema = schema;
    }

    @Override
    public Optional<String> getProductSchema() {
        return schema;
    }

    @Override
    public Optional<String> getColumnName(Connection connection, String logicalTableName, String logicalColumnName) {
        return Optional.of(logicalColumnName);
    }

    @Override
    public Optional<String> getTableName(Connection connection, String logicalTableName) {
        return Optional.of(logicalTableName);
    }
}
