package com.atlassian.pocketknife.internal.querydsl.spring;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.junit.Test;
import org.reflections.Reflections;
import org.reflections.scanners.ResourcesScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toSet;
import static org.hamcrest.CoreMatchers.everyItem;
import static org.hamcrest.Matchers.isIn;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.Assert.assertThat;
import static org.reflections.util.ClasspathHelper.forJavaClassPath;
import static org.reflections.util.FilterBuilder.prefix;
import static org.slf4j.LoggerFactory.getLogger;

public class PocketKnifeQueryDslBeanConfigurationTest {
    private static final Logger log = getLogger(PocketKnifeQueryDslBeanConfigurationTest.class);

    private static final String PK_PKG_PREFIX = "com.atlassian.pocketknife";
    private static final Set<Class<?>> ALL_PK_CLASSES = new Reflections(new ConfigurationBuilder()
            .setScanners(new SubTypesScanner(false), new ResourcesScanner())
            .setUrls(forJavaClassPath())
            .filterInputsBy(new FilterBuilder().include(prefix(PK_PKG_PREFIX))))
            .getSubTypesOf(Object.class);

    @Test
    public void allComponentClasses_shouldHaveACorrespondingBeanEntry() {
        final Set<String> componentClasses = ALL_PK_CLASSES.stream()
                .filter(clazz -> clazz.isAnnotationPresent(Component.class))
                .flatMap(clazz -> {
                    Class<?>[] interfaces = clazz.getInterfaces();
                    if (interfaces.length > 0) {
                        return stream(interfaces);
                    }

                    return stream(new Class<?>[] {clazz});
                })
                .map(Class::getCanonicalName)
                .collect(toSet());

        log.info("All the component classes are: {}", componentClasses);


        final Set<String> beanClasses = stream(PocketKnifeQueryDslBeanConfiguration.class.getDeclaredMethods())
                .map(Method::getReturnType)
                .map(Class::getCanonicalName)
                .collect(toSet());

        log.info("All the bean classes are: {}", beanClasses);

        assertThat(componentClasses, containsInAnyOrder(beanClasses.toArray(new String[0])));
    }

    @Test
    public void allExportedServices_shouldBeAFieldForDocumentation() {
        final Set<String> exportedServices = ALL_PK_CLASSES.stream()
                .filter(clazz -> clazz.isAnnotationPresent(ExportAsService.class))
                .flatMap(clazz -> {
                    Class<?>[] interfaces = clazz.getInterfaces();
                    if (interfaces.length > 0) {
                        return stream(interfaces);
                    }

                    return stream(new Class<?>[] {clazz});
                })
                .map(Class::getCanonicalName)
                .collect(toSet());

        log.info("All the component classes are: {}", exportedServices);


        final Set<String> fieldClasses = stream(PocketKnifeQueryDslBeanConfiguration.class.getDeclaredFields())
                .map(Field::getType)
                .map(Class::getCanonicalName)
                .collect(toSet());

        log.info("All the field classes are: {}", fieldClasses);

        assertThat(exportedServices, everyItem(isIn(fieldClasses.toArray(new String[0]))));
    }

    @Test
    public void allImportedServices_shouldBeAFieldForDocumentation() {
        final Set<String> importedServices = new HashSet<>();
        ALL_PK_CLASSES.forEach(clazz -> {
                    stream(clazz.getFields())
                            .filter(field -> field.isAnnotationPresent(ComponentImport.class))
                            .map(Field::getType)
                            .map(Class::getCanonicalName)
                            .forEach(importedServices::add);

                    stream(clazz.getDeclaredConstructors())
                            .flatMap(constructor -> stream(constructor.getParameters()))
                            .filter(parameter -> parameter.isAnnotationPresent(ComponentImport.class))
                            .map(parameter -> parameter.getType().getTypeName())
                            .forEach(importedServices::add);
                });

        log.info("All the imported services are: {}", importedServices);


        final Set<String> fieldClasses = stream(PocketKnifeQueryDslBeanConfiguration.class.getDeclaredFields())
                .map(Field::getType)
                .map(Class::getCanonicalName)
                .collect(toSet());

        log.info("All the field classes are: {}", fieldClasses);

        assertThat(importedServices, everyItem(isIn(fieldClasses.toArray(new String[0]))));
    }
}
