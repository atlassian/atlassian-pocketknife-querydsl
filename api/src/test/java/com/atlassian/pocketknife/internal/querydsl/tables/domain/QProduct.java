package com.atlassian.pocketknife.internal.querydsl.tables.domain;

import com.atlassian.pocketknife.spi.querydsl.EnhancedRelationalPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;

public class QProduct extends EnhancedRelationalPathBase<QProduct> {

    public NumberPath<Long> ID = createLongCol("ID").asPrimaryKey().build();
    public StringPath NAME = createStringCol("NAME").build();

    public QProduct() {
        super(QProduct.class, "product");
    }
}
