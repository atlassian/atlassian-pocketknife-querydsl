package com.atlassian.pocketknife.internal.querydsl.schema;

import com.atlassian.pocketknife.internal.querydsl.cache.PKQCacheClearer;
import com.atlassian.pocketknife.internal.querydsl.cache.PKQCacheClearerImpl;
import com.atlassian.pocketknife.internal.querydsl.mocks.MockTransactionExecutorFactory;
import com.atlassian.pocketknife.internal.querydsl.tables.TestConnections;
import io.atlassian.fugue.Option;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Optional;

import static java.util.Optional.of;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Unit tests for the {@link DefaultSchemaProviderTest}
 */
public class DefaultSchemaProviderTest {

    private final JdbcTableInspector tableInspector = new JdbcTableInspector();
    private DefaultSchemaProvider classUnderTest;
    private Connection connection;
    private PKQCacheClearer cacheClearer;

    @Before
    public void setup() throws Exception {
        TestConnections.initHSQL();
        connection = TestConnections.getConnection();

        MockTransactionExecutorFactory executorFactory = new MockTransactionExecutorFactory();
        cacheClearer = new PKQCacheClearerImpl(null);
        classUnderTest = new DefaultSchemaProvider(new ProductSchemaProvider(executorFactory), tableInspector, cacheClearer);
        classUnderTest.postConstruct();
        seedDatabase();
    }

    @After
    public void teardown() throws Exception {
        cleanupDatabase();
        TestConnections.close();
    }

    @Test
    public void testGetSchemaWithEmptyProductSchema() throws Exception {

        MockTransactionExecutorFactory executorFactory = new MockTransactionExecutorFactory(Option.none());
        ProductSchemaProvider productSchemaProvider = new ProductSchemaProvider(executorFactory);
        classUnderTest = new DefaultSchemaProvider(productSchemaProvider, tableInspector, cacheClearer);
        assertThat(classUnderTest.getProductSchema(), equalTo(Optional.empty()));
    }

    @Test
    public void testGetSchemaWithNonEmptyProductSchema() throws Exception {
        MockTransactionExecutorFactory executorFactory = new MockTransactionExecutorFactory(Option.some("SCHEMAX"));
        ProductSchemaProvider productSchemaProvider = new ProductSchemaProvider(executorFactory);
        classUnderTest = new DefaultSchemaProvider(productSchemaProvider, tableInspector, cacheClearer);
        assertThat(classUnderTest.getProductSchema(), equalTo(Optional.of("SCHEMAX")));
    }

    @Test
    public void testGetTableNameWithMatchingCase() {
        assertEquals(of("TEST_TABLE"), classUnderTest.getTableName(connection, "TEST_TABLE"));
    }

    @Test
    public void testGetTableNameWithNonMatchingCase() {
        assertEquals(of("TEST_TABLE"), classUnderTest.getTableName(connection, "test_table"));
    }

    @Test
    public void testGetTableNameWithNonExistentTable() {
        assertEquals(Optional.empty(), classUnderTest.getTableName(connection, "missing_table"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetTableNameWithEmptyTable() {
        classUnderTest.getTableName(connection, "");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetTableNameWithNullTable() {
        //noinspection ConstantConditions
        classUnderTest.getTableName(connection, null);
    }

    @Test
    public void testGetColumnNameWithMatchingCase() {
        assertEquals(of("ID"), classUnderTest.getColumnName(connection, "TEST_TABLE", "ID"));
    }

    @Test
    public void testGetColumnNameWithNonMatchingCase() {
        assertEquals(of("ID"), classUnderTest.getColumnName(connection, "test_table", "id"));
    }

    @Test
    public void testGetColumnNameWithNonExistentTable() {
        assertEquals(Optional.empty(), classUnderTest.getColumnName(connection, "missing_table", "id"));
    }

    @Test
    public void testGetColumnNameWithNonExistentColumn() {
        assertEquals(Optional.empty(), classUnderTest.getColumnName(connection, "test_table", "missing_column"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetColumnNameWithEmptyTable() {
        classUnderTest.getColumnName(connection, "", "id");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetColumnNameWithEmptyColumn() {
        classUnderTest.getColumnName(connection, "test_table", "");
    }

    @Test
    public void testCacheClearingHappens() throws Exception {
        classUnderTest.getTableName(connection, "TEST_TABLE");
        assertThat(classUnderTest.getTableAndColumnNames().size() > 0, equalTo(true));

        cacheClearer.clearAllCaches();

        assertThat(classUnderTest.getTableAndColumnNames().size(), equalTo(0));
    }

    private void cleanupDatabase() throws Exception {
        runStatement("DROP TABLE \"TEST_TABLE\" IF EXISTS;");
    }

    private void seedDatabase() throws Exception {
        cleanupDatabase();
        runStatement("CREATE TABLE \"TEST_TABLE\"(\"ID\" BIGINT NOT NULL, \"NAME\" VARCHAR);");
    }


    private void runStatement(String statement) {
        if (statement == null || statement.trim().isEmpty()) {
            return;
        }

        final Connection connection = TestConnections.getConnection();
        try (Statement stmt = connection.createStatement()) {
            stmt.executeUpdate(statement);
            connection.commit();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
