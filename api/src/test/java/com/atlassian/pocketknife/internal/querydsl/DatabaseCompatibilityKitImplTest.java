package com.atlassian.pocketknife.internal.querydsl;

import com.atlassian.pocketknife.api.querydsl.DatabaseAccessor;
import com.atlassian.pocketknife.internal.querydsl.mocks.TestingContext;
import com.atlassian.pocketknife.internal.querydsl.tables.Constants;
import com.atlassian.pocketknife.internal.querydsl.util.Unit;
import com.querydsl.sql.dml.SQLInsertClause;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static com.atlassian.pocketknife.api.querydsl.util.OnRollback.NOOP;
import static com.atlassian.pocketknife.internal.querydsl.tables.domain.QEmployee.employee;
import static org.junit.Assert.assertThat;

public class DatabaseCompatibilityKitImplTest {
    private DatabaseCompatibilityKitImpl databaseCompatibilityKit;
    private DatabaseAccessor databaseAccessor;

    @Before
    public void setUp() throws Exception {
        TestingContext testingContext = TestingContext.forHsql();
        databaseAccessor = testingContext.getDatabaseAccessor();
        databaseCompatibilityKit = new DatabaseCompatibilityKitImpl(testingContext.getDialectConfiguration());
    }

    @Test
    public void test_execute_with_key() throws Exception {
        // arrange
        databaseAccessor.runInNewTransaction(connection -> {
            SQLInsertClause insert = connection.query().insert(employee);
            insert = insert.set(employee.firstname, "First Name").set(employee.lastname, "Last Name").set(employee.salary, new BigDecimal(666)).set(employee.datefield, Constants.date).set(employee.timefield, Constants.time);

            // act
            final Integer id = databaseCompatibilityKit.executeWithKey(connection, insert, Integer.class);

            connection.query().delete(employee).where(employee.id.eq(id)).execute();

            // assert
            assertThat(id, Matchers.notNullValue());

            return Unit.VALUE;
        }, NOOP);
    }
}