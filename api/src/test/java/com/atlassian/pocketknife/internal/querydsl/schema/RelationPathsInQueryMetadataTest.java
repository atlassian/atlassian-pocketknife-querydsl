package com.atlassian.pocketknife.internal.querydsl.schema;

import com.atlassian.pocketknife.internal.querydsl.tables.domain.QProduct;
import com.atlassian.pocketknife.internal.querydsl.tables.domain.QProductAvailability;
import com.atlassian.pocketknife.internal.querydsl.tables.domain.QProductItem;
import com.atlassian.pocketknife.internal.querydsl.tables.domain.QProductItemSku;
import com.querydsl.core.Tuple;
import com.querydsl.sql.RelationalPath;
import com.querydsl.sql.SQLQuery;
import com.querydsl.sql.SQLTemplates;
import org.junit.Test;

import java.util.Set;

import static com.querydsl.sql.SQLExpressions.select;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.junit.Assert.assertThat;

public class RelationPathsInQueryMetadataTest {

    @Test
    public void test_basic_capture() throws Exception {

        QProduct PRODUCT = new QProduct();
        QProductItem PRODUCT_ITEM = new QProductItem();
        QProductItemSku PRODUCT_ITEM_SKU = new QProductItemSku();
        QProductAvailability PRODUCT_AVAILIBILITY = new QProductAvailability();


        SQLQuery<Tuple> sqlQuery = query()
                .select(PRODUCT_ITEM_SKU.SKU_BARCODE, PRODUCT_ITEM.PRICE, PRODUCT.NAME)
                .from(PRODUCT)
                .join(PRODUCT_ITEM)
                .on(PRODUCT_ITEM.PRODUCT_ID.eq(PRODUCT.ID))
                .join(PRODUCT_ITEM_SKU)
                .on(PRODUCT_ITEM_SKU.ID.eq(PRODUCT_ITEM.SKU_ID))
                .where(
                        PRODUCT.NAME.like("drone%")
                                .and(PRODUCT.ID.in(
                                        select(PRODUCT.ID)
                                                .from(PRODUCT)
                                                .join(PRODUCT_AVAILIBILITY)
                                                .on(PRODUCT_AVAILIBILITY.PRODUCT_ID.eq(PRODUCT.ID))
                                                .where(PRODUCT_AVAILIBILITY.AVAILABLE.eq(true))
                                )))
                .orderBy(PRODUCT_ITEM.PRICE.asc());

        Set<RelationalPath<?>> actual = new RelationPathsInQueryMetadata().capture(sqlQuery.getMetadata());

        assertThat(actual.size(), equalTo(4));
        assertThat(actual, hasItem(PRODUCT));
        assertThat(actual, hasItem(PRODUCT_ITEM));

    }

    private SQLQuery<?> query() {
        return new SQLQuery<>(SQLTemplates.DEFAULT);
    }


}