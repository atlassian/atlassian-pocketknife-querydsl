package com.atlassian.pocketknife.internal.querydsl.schema;

import com.atlassian.pocketknife.internal.querydsl.mocks.MockSchemaProvider;
import com.atlassian.pocketknife.internal.querydsl.tables.domain.QProduct;
import com.atlassian.pocketknife.internal.querydsl.tables.domain.QProductItem;
import com.atlassian.pocketknife.internal.querydsl.tables.domain.QProductItemSku;
import com.atlassian.pocketknife.spi.querydsl.EnhancedRelationalPathBase;
import com.google.common.collect.ImmutableSet;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.sql.Configuration;
import com.querydsl.sql.SQLTemplates;
import com.querydsl.sql.SchemaAndTable;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.sql.Connection;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

public class SchemaOverriderTest {

    private static final QProduct PRODUCT = new QProduct();
    private static final QProductItem PRODUCT_ITEM = new QProductItem();
    private static final QProductItemSku PRODUCT_ITEM_SKU = new QProductItemSku();
    private static final QAoTable AO_TABLE = new QAoTable();

    SchemaProvider schemaProvider;
    Connection connection;
    private Configuration qdslConfiguration;
    private SchemaOverrider classUnderTest;

    @Before
    public void setUp() throws Exception {

        connection = mock(Connection.class);
        schemaProvider = new ReverserSchemaProvider();

        qdslConfiguration = new Configuration(SQLTemplates.DEFAULT);
        classUnderTest = new SchemaOverrider(schemaProvider);
    }

    @Test
    public void test_basic_override() throws Exception {

        SchemaAndTable originalProduct = PRODUCT.getSchemaAndTable();
        SchemaAndTable originalProductItem = PRODUCT_ITEM.getSchemaAndTable();
        SchemaAndTable originalProductItemSku = PRODUCT_ITEM_SKU.getSchemaAndTable();

        // starting positions
        assertThat(qdslConfiguration.getOverride(originalProduct), equalTo(originalProduct));
        assertThat(qdslConfiguration.getOverride(originalProductItem), equalTo(originalProductItem));
        assertThat(qdslConfiguration.getOverride(originalProductItemSku), equalTo(originalProductItemSku));

        assertThat(columnOverride(originalProduct, named(PRODUCT.ID)), equalTo("ID"));
        assertThat(columnOverride(originalProduct, named(PRODUCT.NAME)), equalTo("NAME"));

        // act

        qdslConfiguration = classUnderTest.registerOverrides(
                connection,
                qdslConfiguration,
                ImmutableSet.of(PRODUCT, PRODUCT_ITEM));

        // assert

        SchemaAndTable overrideProduct = qdslConfiguration.getOverride(originalProduct);
        SchemaAndTable overrideProductItem = qdslConfiguration.getOverride(originalProductItem);
        SchemaAndTable overrideProductItemSku = qdslConfiguration.getOverride(originalProductItemSku);

        assertThat(overrideProduct, notNullValue());
        assertThat(overrideProductItem, notNullValue());

        assertThat(overrideProduct, not(equalTo(originalProduct)));
        assertThat(overrideProductItem, not(equalTo(originalProductItem)));

        // if its not mentioned then its left alone
        assertThat(overrideProductItemSku, equalTo(originalProductItemSku));

        // did it take effect via the schema provider
        assertThat(overrideProduct.getSchema(), equalTo("SCHEMAX"));
        assertThat(overrideProduct.getTable(), equalTo("tcudorp"));

        // did the columns get changed
        assertThat(columnOverride(originalProduct, named(PRODUCT.ID)), equalTo("DI"));
        assertThat(columnOverride(originalProduct, named(PRODUCT.NAME)), equalTo("EMAN"));
    }

    @Test
    public void test_ao_support() throws Exception {

        SchemaAndTable originalAoTable = AO_TABLE.getSchemaAndTable();

        qdslConfiguration = classUnderTest.registerOverrides(
                connection,
                qdslConfiguration,
                ImmutableSet.of(AO_TABLE));


        SchemaAndTable overrideAoTable = qdslConfiguration.getOverride(originalAoTable);
        assert overrideAoTable != null;

        assertThat(overrideAoTable.getTable(), equalTo("AO_TABLE")); // note the schema provider not called


        assertThat(columnOverride(originalAoTable, named(AO_TABLE.ID)), equalTo("ID"));
        assertThat(columnOverride(originalAoTable, named(AO_TABLE.LOWER_CASE_NAME)), equalTo("LOWERCASE_NAME"));
        assertThat(columnOverride(originalAoTable, named(AO_TABLE.MIXED_CASE_NAME)), equalTo("MIXEDCASE_NAME"));

    }

    private String columnOverride(SchemaAndTable originalProduct, String named) {
        return qdslConfiguration.getColumnOverride(originalProduct, named);
    }

    private String named(Path<?> path) {
        return path.getMetadata().getName();
    }

    static class QAoTable extends EnhancedRelationalPathBase<QAoTable> {

        public NumberPath<Long> ID = createLongCol("ID").asPrimaryKey().build();
        public StringPath LOWER_CASE_NAME = createStringCol("lowercase_name").build();
        public StringPath MIXED_CASE_NAME = createStringCol("MixedCase_name").build();

        QAoTable() {
            super(QAoTable.class, "AO_Table");
        } // mixed case does not matter - it will be handled
    }

    @ParametersAreNonnullByDefault
    class ReverserSchemaProvider extends MockSchemaProvider {


        @Override
        public Optional<String> getColumnName(Connection connection, String logicalTableName, String logicalColumnName) {
            return Optional.of(reverseString(logicalColumnName));
        }

        @Override
        public Optional<String> getProductSchema() {
            return Optional.of("SCHEMAX");
        }

        @Override
        public Optional<String> getTableName(Connection connection, String logicalTableName) {
            return Optional.of(reverseString(logicalTableName));
        }
    }


    @Nullable
    private static String reverseString(@Nullable String input) {
        if (input == null) {
            return null;
        }

        return new StringBuilder(input).reverse().toString();
    }
}
