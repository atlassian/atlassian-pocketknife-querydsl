package com.atlassian.pocketknife.internal.querydsl.stream;

import com.atlassian.pocketknife.api.querydsl.stream.ClosePromise;
import com.atlassian.pocketknife.api.querydsl.stream.CloseableIterable;
import com.atlassian.pocketknife.api.querydsl.stream.CloseableIterables;
import com.atlassian.pocketknife.internal.querydsl.stream.PartitionedCloseableIterable.PartitionedCloseableIterator;
import com.google.common.collect.ImmutableList;
import com.mysema.commons.lang.CloseableIterator;
import com.querydsl.core.types.ConstantImpl;
import com.querydsl.core.types.Expression;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.Collections.emptyList;
import static java.util.Optional.empty;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class PartitionedCloseableIterableTest {

    private static final List<Integer> RESULTS_1 = ImmutableList.of(1, 2);
    private static final List<Integer> RESULTS_2 = ImmutableList.of(3, 4);

    private CloseableIterable<Integer> closeableIterable1;
    private CloseableIterable<Integer> closeableIterable2;

    @Mock
    private CloseableIterator<Integer> closeableIterator1;

    @Mock
    private CloseableIterator<Integer> closeableIterator2;

    @Mock
    private ClosePromise closePromise1;

    @Mock
    private ClosePromise closePromise2;

    @Before
    public void setup() {
        closeableIterable1 = CloseableIterables.iterable(closeableIterator1, closePromise1);
        closeableIterable2 = CloseableIterables.iterable(closeableIterator2, closePromise2);

        final List<Integer> results1 = newArrayList(RESULTS_1);
        final List<Integer> results2 = newArrayList(RESULTS_2);

        doAnswer(invocation -> !results1.isEmpty()).when(closeableIterator1).hasNext();
        doAnswer(invocation -> !results2.isEmpty()).when(closeableIterator2).hasNext();

        doAnswer(invocation -> results1.remove(0)).when(closeableIterator1).next();
        doAnswer(invocation -> results2.remove(0)).when(closeableIterator2).next();
    }

    @Test
    public void null__is_treated_as_empty_list() {
        final PartitionedCloseableIterable<Integer> closeableIterable = new PartitionedCloseableIterable<>(null);

        assertThat(closeableIterable.fetchFirst(), is(empty()));
    }

    @Test
    public void iterator__returns_partitioned_iterator_for_single_list() {
        final PartitionedCloseableIterable<Integer> closeableIterable = new PartitionedCloseableIterable<>(ImmutableList.of(closeableIterable1));

        final CloseableIterator<Integer> iterator = closeableIterable.iterator();

        assertThat(iterator, instanceOf(PartitionedCloseableIterator.class));

        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is(1));
        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is(2));
        assertThat(iterator.hasNext(), is(false));
    }

    @Test
    public void iterator__returns_partitioned_iterator_for_multiple_lists() {
        final PartitionedCloseableIterable<Integer> closeableIterable = new PartitionedCloseableIterable<>(ImmutableList.of(closeableIterable1, closeableIterable2));

        final CloseableIterator<Integer> iterator = closeableIterable.iterator();

        assertThat(iterator, instanceOf(PartitionedCloseableIterator.class));

        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is(1));
        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is(2));
        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is(3));
        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is(4));
        assertThat(iterator.hasNext(), is(false));
    }

    @Test
    public void fetchFirst__empty_optional_for_empty_list() {
        final PartitionedCloseableIterable<Integer> closeableIterable = new PartitionedCloseableIterable<>(emptyList());

        assertThat(closeableIterable.fetchFirst(), is(empty()));
    }

    @Test
    public void fetchFirst__returns_first_value_for_single_list() {
        final PartitionedCloseableIterable<Integer> closeableIterable = new PartitionedCloseableIterable<>(ImmutableList.of(closeableIterable1));

        assertThat(closeableIterable.fetchFirst(), is(Optional.of(1)));

        // close called
        verify(closePromise1, times(2)).close();
    }

    @Test
    public void fetchFirst__returns_first_value_for_multiple_lists() {
        final PartitionedCloseableIterable<Integer> closeableIterable = new PartitionedCloseableIterable<>(ImmutableList.of(closeableIterable1, closeableIterable2));

        assertThat(closeableIterable.fetchFirst(), is(Optional.of(1)));

        // close called
        verify(closePromise1, times(2)).close();
        verify(closePromise2, times(2)).close();
    }

    @Test
    public void take__returns_new_iterable_for_single_list() {
        final PartitionedCloseableIterable<Integer> closeableIterable = new PartitionedCloseableIterable<>(ImmutableList.of(closeableIterable1));

        final CloseableIterable<Integer> iterable = closeableIterable.take(1);

        assertThat(iterable, instanceOf(PartitionedCloseableIterable.class));

        final CloseableIterator<Integer> iterator = iterable.iterator();

        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is(1));
        assertThat(iterator.hasNext(), is(false));
    }

    @Test
    public void take__returns_new_iterable_for_multiple_lists() {
        final PartitionedCloseableIterable<Integer> closeableIterable = new PartitionedCloseableIterable<>(ImmutableList.of(closeableIterable1, closeableIterable2));

        final CloseableIterable<Integer> iterable = closeableIterable.take(3);

        assertThat(iterable, instanceOf(PartitionedCloseableIterable.class));

        final CloseableIterator<Integer> iterator = iterable.iterator();

        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is(1));
        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is(2));
        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is(3));
        assertThat(iterator.hasNext(), is(false));
    }

    @Test
    public void take__handles_request_of_more_values_than_have() {
        final PartitionedCloseableIterable<Integer> closeableIterable = new PartitionedCloseableIterable<>(ImmutableList.of(closeableIterable1, closeableIterable2));

        final CloseableIterable<Integer> iterable = closeableIterable.take(30);

        assertThat(iterable, instanceOf(PartitionedCloseableIterable.class));

        final CloseableIterator<Integer> iterator = iterable.iterator();

        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is(1));
        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is(2));
        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is(3));
        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is(4));
        assertThat(iterator.hasNext(), is(false));
    }

    @Test
    public void takeWhile__returns_new_iterable_for_single_list() {
        final PartitionedCloseableIterable<Integer> closeableIterable = new PartitionedCloseableIterable<>(ImmutableList.of(closeableIterable1));

        final CloseableIterable<Integer> iterable = closeableIterable.takeWhile(number -> number % 2 == 1);

        assertThat(iterable, instanceOf(PartitionedCloseableIterable.class));

        final CloseableIterator<Integer> iterator = iterable.iterator();

        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is(1));
        assertThat(iterator.hasNext(), is(false));
    }

    @Test
    public void takeWhile__returns_new_iterable_for_multiple_list() {
        final PartitionedCloseableIterable<Integer> closeableIterable = new PartitionedCloseableIterable<>(ImmutableList.of(closeableIterable1, closeableIterable2));

        final CloseableIterable<Integer> iterable = closeableIterable.takeWhile(number -> number % 2 == 1);

        assertThat(iterable, instanceOf(PartitionedCloseableIterable.class));

        final CloseableIterator<Integer> iterator = iterable.iterator();

        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is(1));
        assertThat(iterator.hasNext(), is(false));
    }

    @Test
    public void takeWhile__returns_new_iterable_for_multiple_list2() {
        final PartitionedCloseableIterable<Integer> closeableIterable = new PartitionedCloseableIterable<>(ImmutableList.of(closeableIterable1, closeableIterable2));

        final CloseableIterable<Integer> iterable = closeableIterable.takeWhile(number -> number <= 3);

        assertThat(iterable, instanceOf(PartitionedCloseableIterable.class));

        final CloseableIterator<Integer> iterator = iterable.iterator();

        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is(1));
        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is(2));
        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is(3));
        assertThat(iterator.hasNext(), is(false));
    }

    @Test
    public void takeWhile__returns_new_iterable_even_when_no_results() {
        final PartitionedCloseableIterable<Integer> closeableIterable = new PartitionedCloseableIterable<>(ImmutableList.of(closeableIterable1, closeableIterable2));

        final CloseableIterable<Integer> iterable = closeableIterable.takeWhile(number -> number > 30);

        assertThat(iterable, instanceOf(PartitionedCloseableIterable.class));

        final CloseableIterator<Integer> iterator = iterable.iterator();

        assertThat(iterator.hasNext(), is(false));
    }

    @Test
    public void filter__returns_new_iterable_when_single_list() {
        final PartitionedCloseableIterable<Integer> closeableIterable = new PartitionedCloseableIterable<>(ImmutableList.of(closeableIterable1));

        final CloseableIterable<Integer> iterable = closeableIterable.filter(number -> number % 2 == 0);

        assertThat(iterable, instanceOf(PartitionedCloseableIterable.class));

        final CloseableIterator<Integer> iterator = iterable.iterator();

        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is(2));
        assertThat(iterator.hasNext(), is(false));
    }

    @Test
    public void filter__returns_new_iterable_when_multiple_lists() {
        final PartitionedCloseableIterable<Integer> closeableIterable = new PartitionedCloseableIterable<>(ImmutableList.of(closeableIterable1, closeableIterable2));

        final CloseableIterable<Integer> iterable = closeableIterable.filter(number -> number % 2 == 0);

        assertThat(iterable, instanceOf(PartitionedCloseableIterable.class));

        final CloseableIterator<Integer> iterator = iterable.iterator();

        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is(2));
        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is(4));
        assertThat(iterator.hasNext(), is(false));
    }

    @Test
    public void filter__returns_new_iterable_when_no_results() {
        final PartitionedCloseableIterable<Integer> closeableIterable = new PartitionedCloseableIterable<>(ImmutableList.of(closeableIterable1, closeableIterable2));

        final CloseableIterable<Integer> iterable = closeableIterable.filter(number -> number > 30);

        assertThat(iterable, instanceOf(PartitionedCloseableIterable.class));

        final CloseableIterator<Integer> iterator = iterable.iterator();

        assertThat(iterator.hasNext(), is(false));
    }

    @Test
    public void map__returns_new_iterable_with_function_for_single_list() {
        final PartitionedCloseableIterable<Integer> closeableIterable = new PartitionedCloseableIterable<>(ImmutableList.of(closeableIterable1));

        final CloseableIterable<Integer> iterable = closeableIterable.map(number -> number + 10);

        assertThat(iterable, instanceOf(PartitionedCloseableIterable.class));

        final CloseableIterator<Integer> iterator = iterable.iterator();

        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is(11));
        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is(12));
        assertThat(iterator.hasNext(), is(false));
    }

    @Test
    public void map__returns_new_iterable_with_function_for_multiple_lists() {
        final PartitionedCloseableIterable<Integer> closeableIterable = new PartitionedCloseableIterable<>(ImmutableList.of(closeableIterable1, closeableIterable2));

        final CloseableIterable<Integer> iterable = closeableIterable.map(number -> number + 10);

        assertThat(iterable, instanceOf(PartitionedCloseableIterable.class));

        final CloseableIterator<Integer> iterator = iterable.iterator();

        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is(11));
        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is(12));
        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is(13));
        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is(14));
        assertThat(iterator.hasNext(), is(false));
    }

    @Test
    public void map__expression_verify_that_called_single_list() {
        final Expression<Integer> integerConstant = ConstantImpl.create(1);

        closeableIterable1 = mock(CloseableIterable.class);

        doAnswer(invocation -> invocation.getMock()).when(closeableIterable1).map(integerConstant);

        // testing this is a lot harder to verify the new iterable. Instead for this, only verifying that call map on all
        final PartitionedCloseableIterable<Integer> closeableIterable = new PartitionedCloseableIterable<>(ImmutableList.of(closeableIterable1));

        final CloseableIterable<Integer> iterable = closeableIterable.map(integerConstant);

        assertThat(iterable, instanceOf(PartitionedCloseableIterable.class));

        verify(closeableIterable1).map(integerConstant);
    }

    @Test
    public void map__expression_verify_that_called_multiple_lists() {
        final Expression<Integer> integerConstant = ConstantImpl.create(1);

        closeableIterable1 = mock(CloseableIterable.class);
        closeableIterable2 = mock(CloseableIterable.class);

        doAnswer(invocation -> invocation.getMock()).when(closeableIterable1).map(integerConstant);
        doAnswer(invocation -> invocation.getMock()).when(closeableIterable2).map(integerConstant);

        // testing this is a lot harder to verify the new iterable. Instead for this, only verifying that call map on all
        final PartitionedCloseableIterable<Integer> closeableIterable = new PartitionedCloseableIterable<>(ImmutableList.of(closeableIterable1, closeableIterable2));

        final CloseableIterable<Integer> iterable = closeableIterable.map(integerConstant);

        assertThat(iterable, instanceOf(PartitionedCloseableIterable.class));

        verify(closeableIterable1).map(integerConstant);
        verify(closeableIterable2).map(integerConstant);
    }

    @Test
    public void foldLeft__works_over_single_list() {
        final PartitionedCloseableIterable<Integer> closeableIterable = new PartitionedCloseableIterable<>(ImmutableList.of(closeableIterable1));

        final Long result = closeableIterable.foldLeft(20L, (accum, number) -> accum + number);

        assertThat(result, is(23L));
    }

    @Test
    public void foldLeft__works_over_multiple_lists() {
        final PartitionedCloseableIterable<Integer> closeableIterable = new PartitionedCloseableIterable<>(ImmutableList.of(closeableIterable1, closeableIterable2));

        final Long result = closeableIterable.foldLeft(20L, (accum, number) -> accum + number);

        assertThat(result, is(30L));
    }

    @Test
    public void forEach__works_over_single_list() {
        final PartitionedCloseableIterable<Integer> closeableIterable = new PartitionedCloseableIterable<>(ImmutableList.of(closeableIterable1));

        final AtomicLong counter = new AtomicLong(0);

        closeableIterable.foreach(counter::addAndGet);

        assertThat(counter.get(), is(3L));
    }

    @Test
    public void forEach__works_over_multiple_lists() {
        final PartitionedCloseableIterable<Integer> closeableIterable = new PartitionedCloseableIterable<>(ImmutableList.of(closeableIterable1, closeableIterable2));

        final AtomicLong counter = new AtomicLong(0);

        closeableIterable.foreach(counter::addAndGet);

        assertThat(counter.get(), is(10L));
    }

    @Test
    public void close__works_over_single_list() {
        final PartitionedCloseableIterable<Integer> closeableIterable = new PartitionedCloseableIterable<>(ImmutableList.of(closeableIterable1));

        closeableIterable.close();

        verify(closePromise1, times(2)).close();
    }

    @Test
    public void close__works_over_multiple_lists() {
        final PartitionedCloseableIterable<Integer> closeableIterable = new PartitionedCloseableIterable<>(ImmutableList.of(closeableIterable1, closeableIterable2));

        closeableIterable.close();

        verify(closePromise1, times(2)).close();
        verify(closePromise2, times(2)).close();
    }

    @Test
    public void take_then_close__closes_all_original_lists() {
        final PartitionedCloseableIterable<Integer> closeableIterable = new PartitionedCloseableIterable<>(ImmutableList.of(closeableIterable1, closeableIterable2));

        final CloseableIterable<Integer> iterable = closeableIterable.take(1);

        assertThat(iterable, instanceOf(PartitionedCloseableIterable.class));

        final CloseableIterator<Integer> iterator = iterable.iterator();

        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is(1));
        assertThat(iterator.hasNext(), is(false));

        // close the taken iterable
        iterable.close();

        verify(closePromise1, times(1)).close();
        verify(closePromise2, times(1)).close();
    }

    @Test
    public void takeWhile_then_close__closes_all_original_lists() {
        final PartitionedCloseableIterable<Integer> closeableIterable = new PartitionedCloseableIterable<>(ImmutableList.of(closeableIterable1, closeableIterable2));

        final CloseableIterable<Integer> iterable = closeableIterable.takeWhile(number -> number == 1);

        assertThat(iterable, instanceOf(PartitionedCloseableIterable.class));

        final CloseableIterator<Integer> iterator = iterable.iterator();

        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is(1));
        assertThat(iterator.hasNext(), is(false));

        // close the taken iterable
        iterable.close();

        verify(closePromise1).close();
        verify(closePromise2).close();
    }

    @Test
    public void filter_then_close__closes_all_original_lists() {
        final PartitionedCloseableIterable<Integer> closeableIterable = new PartitionedCloseableIterable<>(ImmutableList.of(closeableIterable1, closeableIterable2));

        final CloseableIterable<Integer> iterable = closeableIterable.filter(number -> number == 1);

        assertThat(iterable, instanceOf(PartitionedCloseableIterable.class));

        final CloseableIterator<Integer> iterator = iterable.iterator();

        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is(1));
        assertThat(iterator.hasNext(), is(false));

        // close the taken iterable
        iterable.close();

        verify(closePromise1, times(1)).close();
        verify(closePromise2, times(1)).close();
    }

    @Test
    public void map_then_close__closes_all_original_lists() {
        final PartitionedCloseableIterable<Integer> closeableIterable = new PartitionedCloseableIterable<>(ImmutableList.of(closeableIterable1, closeableIterable2));

        final CloseableIterable<Integer> iterable = closeableIterable.map(number -> number + 1);

        assertThat(iterable, instanceOf(PartitionedCloseableIterable.class));

        final CloseableIterator<Integer> iterator = iterable.iterator();

        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is(2));
        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is(3));
        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is(4));
        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is(5));
        assertThat(iterator.hasNext(), is(false));

        // close the taken iterable
        iterable.close();

        verify(closePromise1, times(2)).close();
        verify(closePromise2, times(2)).close();
    }
}
