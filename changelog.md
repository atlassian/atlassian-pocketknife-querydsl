# Change Log
All notable changes to this project will attempt to be documented in this file.

## [8.0.0]

- [BORG-223] Updated querydsl to v5

## [5.2.1]
### Removed
- removed references to `com.atlassian.annotation.tenancy`

## [5.1.0] - [2023-05-12]

### Added
- [BSP-5045] Support for Spring Java config

## [5.0.6] - [2023-05-04]

### Fixed
- Removal of Apache commons usages, unfortunate that AO still has it though. Makes it easy to be sure a plugin doesn't need it

## [4.4.1] - [2017-06-14]

### Changed
- org.jooq:joor has been changed to compile scope as it was being included on that scope transitively through bridge dependency previously

## [4.4.0] - [2017-06-14]

### Added
- OnRollback functional interface has been added, with the intention to allow the caller to perform actions when a rollback occurs, without having to understand the possible ways it may actually occur.
- All Database Accessors (DatabaseAccessor, OptionalAwareDatabaseAccessor, EitherAwareDatabaseAccessor) added new runner methods that take an OnRollback callback parameter.

### Deprecated
- All Database Accessors (DatabaseAccessor, OptionalAwareDatabaseAccessor, EitherAwareDatabaseAccessor) deprecated runner methods that do not accept OnRollback parameter. Intention is to make obvious to callers that rollback scenario needs to be considered, even if NOOP. Should be removed in 5.0.0

## [4.3.0] - [2017-06-09]

### Added
- OptionalAwareDatabaseAccessor to allow transaction management using Optional where empty result means rollback
- EitherAwareDatabaseAccessor to allow transaction management using Either where left result means rollback

### Changed
- DatabaseAccessor#run contract has changed to be a synonym of runInTransaction (instead of runInNewTransaction). This change makes the default run method safer to use, in the common event of nested usage.
- Update querydsl library dependency from 4.0.3 to 4.1.4. No code changes required as a result of bump.
- Update atlassian-spring-scanner to 2.0.0
- Update JIRA version for test plugin to run against to 7.3.0

### Removed
- Removed bridge code that handled Cloud vs Server as this repository is now Server only

# Issues

* [BSP-5045](https://bulldog.internal.atlassian.com/browse/BSP-5045)
